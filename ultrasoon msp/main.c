
#include <msp430.h>

int miliseconden;
int cm;
long ECHO;

void main(void)
{
  BCSCTL1 = CALBC1_1MHZ;
  DCOCTL = CALDCO_1MHZ;                                     // submainclock 16mhz
  WDTCTL = WDTPW + WDTHOLD;                                 // Stop Watch Dog Timer

  CCTL0 = CCIE;                                             // CCR0 interrupt enabled
  CCR0 = 1000;                                              // 1ms at 16mhz
  TACTL = TASSEL_2 | MC_1;                                  // SMCLK, upmode, interrupt enable

  P2SEL &= ~(1<<6 | 1<<7);

  P2IFG  = 0x00;                                            //clear all interrupt flags

  P1DIR |= (1<<0);
//  P1OUT &= (1<<0);


  _BIS_SR(GIE);                                             // globale interrupt enable

  while(1)
    {
        P2DIR |= (1<<7);                                    // trigger pin as output P2.6

        P2OUT |= (1<<7);                                    // Puls genereren
        __delay_cycles(10);                                 // voor 10us
        P2OUT &= ~(1<<7);                                   // stop puls

        P2DIR &= ~(1<<6);                                   // maak pin P2.6 een input (ECHO)
        P2IE |= (1<<6);                                     // enable interupt op ECHO pin
        P2IES &= ~(1<<6);                                   // rising edge op ECHO pin

        __delay_cycles(30000);                              // vertraging voor 30ms (na deze tijd vergaat de echo en is er geen object gedetecteerd)

        cm = (ECHO / 58);                               // converteren lengte in cm

        if((cm <= 20) && (cm >= 8))
        {
            P1OUT |= (1<<0);
        }
        else if(cm >= 21)
        {
            P1OUT &= ~(1<<0);
        }
    }
}

#pragma vector = PORT2_VECTOR
__interrupt void Port_2(void)
{
    if(P2IFG & 1<<6)                                        //is er een interrupt?
        {
          if(!(P2IES & 1<<6))                               //is het rising edge?
          {
            TACTL|=TACLR;                                   // clears timer A
            miliseconden = 0;
            P2IES |= 1<<6;                                  //falling edge
          }
          else
          {
            ECHO = (long)miliseconden*1000 + (long)TA0R;   //calculeren ECHO lengte
          }
    P2IFG &= ~(1<<6);                                       //clear flag
    }
}


#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
    miliseconden++;
}
