//#include <msp430.h>
//
//#pragma vector = PORT1_VECTOR
//__interrupt void mijnADCInterruptRoutine(void)
//{
//    if ((P1IFG & (1 << 2)) == (1 << 2) && (P1IFG & (1<<4))==(1<<4))
//    {
//        P2OUT &= ~(1 << 1);           // Zet motor 1 uit
//        P2OUT &= ~(1 << 0);
//        P2OUT &= ~(1 << 3);           // zet motor 2 uit
//        P2OUT &= ~(1 << 4);
//
//        P1IFG &= ~((1 << 4)|(1<<2));
//
//    }
//
//    if ((P1IFG & (1 << 2)))
//    {
//        // AAN
//        P2OUT &= ~(1 << 1);           // Zet motor 1 uit
//        P2OUT &= ~(1 <<0);
//        //P2OUT = 1<<1;
//        //P2OUT = 1<<0;
//
//        P1IFG &= ~(1 << 2);
//    }
//
//    if ((P1IFG & (1 << 4)))
//    {
//        //UIT
//        P2OUT &= ~(1 << 3);           // zet motor 2 uit
//        P2OUT &= ~(1 << 4);
//
//        P1IFG &= ~(1 << 4);
//    }
//    // deze code wordt uitgevoerd bij een ADC interrupt
//}
//
///**
// * main.c
// */
//int main(void)
//{
//    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer
//    // reset registers
//
//    //interrupts
//    P1IFG = 0x00;
//    P1IE = (1 << 2) | (1 << 4);
//    //P1IES = 0x00;
//    P1IES = (1 << 2) | (1 << 4);
//
//    __enable_interrupt();
//
//    P1DIR = 0;
//    P2DIR = 0;
//    /*
//     P1SEL = 0;
//     P1SEL1 = 0;
//     P2SEL = 0;
//     P2SEL1 = 0;
//     */
//    P1OUT = 0;
//    P2OUT = 0;
//
//    // end reset registers
//
//    // code vincent
//
//    P2DIR |= (1 << 2) | (1 << 5); //PIN HOOG OUTPUT
//    P2SEL |= (1 << 2) | (1 << 5); // OUTPUT CLOCK GAAT NAAR DE OUTPUT PIN
//
//    if (CALBC1_1MHZ == 0xFF)
//    {
//        while (1)
//            ; // Doe niets
//    }
//
//    /*
//     * set clock frequentie on 1Mhz
//     */
//    DCOCTL = 0;
//    BCSCTL1 = CALBC1_1MHZ;
//    DCOCTL = CALDCO_1MHZ;
//
//    TA1CTL = TASSEL_2 | ID_3 | MC_1;
//
//    TA1CCR0 = 600;
//
//    TA1CCTL1 = OUTMOD_7;
//    TA1CCR1 = 200; //Hoe hoger hoe langzamer motor 1(links)
//
//    TA1CCTL2 = OUTMOD_7;
//    TA1CCR2 = 200; //Hoe hoger hoe langzamer motor 2(rechts)
//
//    // end timer settings
//
//    P2DIR |= (1 << 0) | (1 << 1) | (1 << 3) | (1 << 4);
//
//    while (1)
//    {
//        __delay_cycles(20000);
//        // Motor 1 aan links
//        P2OUT |= (1 << 1);        // Vooruit
//        P2OUT &= ~(1 << 0);       // Achteruit
//
//
//        // Motor 2 aan rechts
//        P2OUT |= (1 << 3);       // Vooruit
//        P2OUT &= ~(1 << 4);       // Achteruit
//    }
//    // end code vincent
//
//    return 0;
//}
