#include <msp430.h>
#include <stdint.h>
#include <string.h>
#include <SPI.h>
#include <Mfrc522.h>
#include "../Lib/gpio.h"
//#include "../Lib/i2c.h"
#include "../Lib/oled.h"
#include "../Lib/fonts.h"
#include "Uart.h"

//todo implement new driving code

#define Slave_Msp_Addr 0xAA

#define RC522_CS_PIN (1<<4) // P1.4
#define RC522_RST_PIN (1<<4) //P2.4

/** main.c
 *
 * pinout cpu 1
 *
 * P1.0 Rotary1 -- encoder engine 1
 * P1.1 SPI_miso
 * P1.2 SPI_mosi
 * P1.3 Rotary2 -- encoder engine 2
 * P1.4 SPI_STE -- nfc sda pin select
 * P1.5 SPI_CLK -- nfc sck clock
 * P1.6 I2C_SCL -- I2C SCL
 * P1.7 I2C_SDA -- I2C SDA
 *
 * P2.0 IRlijn1 -- ir sensor lijn 1
 * P2.1 IRlijn2 -- ir sensor lijn 2
 * P2.2 HbrugPWM1 -- hbrug pwm output 1
 * P2.3 IRlijn3 -- ir sensor lijn 3
 * P2.4 NFC reset  //wordt sensor 4
 * P2.5 HbrugPWM2 -- hbrug pwm output 2
 * P2.6 Ultrasoon ingang  // ook wel XIN.
 * P2.7 UStrig  // ook wel XOUT.
 */

uint8_t distance = 0xAA;
uint8_t Route[] = "RLRDRDDDRLRRDDDRS"; // array voor richting , Boas
int r = 0; // array identifier voor richting, Boas
int ultra = 0;
uint8_t speed = 50;
uint8_t start = 0;
uint8_t twoComplements = 0xAA;
uint8_t Color = 'R';

uint8_t recvdata;
int messcount = 0;
actions action = NONE;

void ProcessData(){
    if(action == NONE){
        action = recvdata;
    }
    else{
        switch(action){
        case SetDirections:
            //Route[messcount] = recvdata;
            if(recvdata == 'S'){
                action = NONE;
                messcount = 0;
            }
            break;
        case SetSpeed:
            action = NONE;
            break;
        case Start:
            start = 1;
            UART_putc(GetDone);
            UART_putc(start);
            action = NONE;
            break;
        case Stop:
            start = 0;
            UART_putc(GetDone);
            UART_putc(start);
            action = NONE;
            break;
        default:
            break;
        }
    }
}

#pragma vector = USCIAB0RX_VECTOR
__interrupt void UART_RX_Interrupt(){
    if(IFG2 & UCA0RXIFG){
        recvdata = UCA0RXBUF;
        ProcessData();
    }
}

int millis = 0;
int seconden = 0;

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer_A(void)
{
    millis++;
}

typedef enum
{
    Left, Right
} Side;
typedef enum
{
    Off, On
} Action;

void Motor(Side side, Action action, int pwm);
void NFC_reader(void);
float GemiddeldeAfstand(void);
void Route_Afleggen(void);

int stop; //Gerben, wordt niet meer naar de lijn sensoren gekeken als deze een waarde krijgt
int Ultra; //Gerben, zorgt ervoor dat als de ultrasoon iets detecteert er geen route afgelegd kan worden, wordt er niet meer naar de sensoren gekeken

int PWMSpeed = 108; // snelheden PWM
int PWMlinks = 88;
int PWMrechts = 86;
int PWMbijR = 50;
int PWMbijL = 54;
int PWMDraai = 85;
int PWMBocht = 100;

int route = 0;          // Gerben, Route int.
float Afstand;      // Vincent, Encoder
int bekijkroute=0;

int klik=0;
int klik2=0;

char recvData[18];  //Roy, NFC tag ontvang data
int h;              //Roy,
unsigned char NFCledkleur; //Roy, variabel die de NFC kleur overneemt

#pragma vector=PORT1_VECTOR
__interrupt void encoder(void)
{
    if (P1IFG & (1<<0))
    {
        klik++;
        P1IFG &= ~(1<<0);
    }
    if (P1IFG & (1<<3))
    {
        klik2 ++;
        P1IFG &= ~(1<<3);
    }
}



#pragma vector = PORT2_VECTOR
__interrupt void port2Interrupt(void)
{
    if(start == 1)
    {
        if (((P2IN & (1 << 1)) ==(1<<1)) && ((P2IN & (1 << 3)) ==(1<<3)) && ((P2IN & (1 << 0))==(1<<0)) && (bekijkroute==0)) // Als hij een kruispunt midden in het raster ziet
        {
            stop = 1;
            Motor(Right, Off, PWMSpeed);
            Motor(Left, Off, PWMSpeed);

            if (Route[r] == 'L')//array L) // als hij links af moet slaan
            {
                bekijkroute = 1;
                route = 1;
                //Route_Afleggen();
                r++;
     //           P2IFG &= ~((1 << 0) | (1<<3));
            }
            else if (Route[r] == 'R')//array R) //  als hij rechts af moet slaan
            {
                bekijkroute = 1;
                route=2;
                //Route_Afleggen();
                r++;
     //           P2IFG &= ~((1 << 0) | (1<<3));
            }
            else if ((Route[r] == 'D') && ((P2IN & (1<<4)) == (1<<4)))//array D) //  als hij rechtdoor moet gaan.
            {
                bekijkroute = 1;
                route = 0;
                //Route_Afleggen();
                r++;
       //         P2IFG &= ~((1 << 0) | (1<<3));
            }
            else if (Route[r] == 'S')//array S) // als hij moet stoppen.
            {
                bekijkroute = 1;
                route = 3;
                //Route_Afleggen();
   //             P2IFG &= ~((1 << 0) | (1<<3));
            }
        }
        if (((P2IN & (1 << 1)) == (1<<1)) && ((P2IN & (1 << 0))==(1<<0)) && (bekijkroute==0) && ((P2IN & (1<<4))==(1<<4))) // Als hij een kruispunt en de rechter lijn niet zie buiten het raster zie
        {
        stop = 1;
        Motor(Right, Off, PWMSpeed);
        Motor(Left, Off, PWMSpeed);

            if (Route[r] == 'L')//array L) //  als hij links af moet slaan
            {
                bekijkroute = 1;
                route = 0;
                //Route_Afleggen();
                r++;
   //             P2IFG &= ~((1 << 0) | (1<<3));
            }
            else if (Route[r] == 'D')//array D) // als hij door af moet slaan
            {
                bekijkroute = 1;
                route = 1;
                //Route_Afleggen();
                r++;
  //              P2IFG &= ~((1 << 0) | (1<<3));
            }
            else
            {
                stop=0;
            }
        }
        if (((P2IN & (1 << 1)) == (1<<1)) && ((P2IN & (1 << 3))==(1<<3)) && (bekijkroute==0) && ((P2IN & (1<<4))==(1<<4))) // Als hij een kruispunt en de linker lijn niet zie buiten het raster zie
        {
        stop = 1;
        Motor(Right, Off, PWMSpeed);
        Motor(Left, Off, PWMSpeed);

            if (Route[r] == 'D')//array D)
            {
                bekijkroute = 1;
                route = 0;
                //Route_Afleggen();
                r++;
  //              P2IFG &= ~((1 << 3) | (1<<0));
            }
            else if (Route[r] == 'R')//array R) //  als hij rechts af moet slaan
            {
                bekijkroute = 1;
                route = 2;
                //Route_Afleggen();
                r++;
  //              P2IFG &= ~((1 << 0) | (1<<3));
            }
            else
            {
                stop=0;
            }
        }
        if ((stop == 0) && (bekijkroute==0)) // voorkomen dat hij doorgaat met rijden als die een kruispunt of de ultrasoon iets detecteert.
        {
            if ((P2IN & (1 << 0))==(1<<0)) // linker sensor detecteert
            {
                Motor(Left,On, PWMbijL);
                Motor(Right,On, PWMrechts);
            }
            if ((P2IN & (1 << 3))==(1<<3)) // rechter sensor detecteert
            {
                Motor(Right, On, PWMbijR);
                Motor(Left,On, PWMlinks);
            }
     //       P2IFG &= ~((1 << 0) | (1<<3));
        }
    }
    else{
        Motor(Right,Off,0);
        Motor(Left,Off,0);
    }
    P2IFG &= ~((1 << 0) | (1<<3));
    P2IFG &= ~(1<<1);
}

void Route_Afleggen(void)

{
    millis=0;
    seconden = 0;
    while (route == 3 ) // Stop
    {
        Motor(Right, Off, PWMrechts);
        Motor(Left, Off, PWMlinks);
        start = 0;
        UART_putc(GetDone);
        UART_putc(start);
    }
    while (route == 1 ) // Links
    {

        Motor(Right, On, PWMrechts);
        Motor(Left, Off, PWMbijL);
        if (((P2IN & (1<<0)) != (1<<0)) && ((P2IN & (1<<1)) == (1<<1)) && (((P2IN & (1<<3)) != (1<<3))) && ((P2IN & (1<<4)) == (1<<4)) && (millis >600))
        {
           Motor(Left, On, PWMSpeed);
           route = 0;
           stop=0;
           bekijkroute=0;
        }
    }
    while (route == 2 ) // Rechts
    {

        Motor(Right, Off, PWMbijR);
        Motor(Left, On, PWMlinks);
        if (((P2IN & (1<<0)) != (1<<0)) && ((P2IN & (1<<1)) == (1<<1)) && (((P2IN & (1<<3)) != (1<<3))) && ((P2IN & (1<<4)) == (1<<4)) && (millis >600))
        {
            Motor(Right, On, PWMSpeed);
            route = 0;
            stop=0;
            bekijkroute=0;
        }
    }
    if (route == 0 ) // Midden
    {
        stop = 0;
        bekijkroute=0;
    }

}

void SetupEncoder(void)
{
    P1DIR &= ~((1<<0)|(1<<3));
    P1REN = (1<<0) | (1<<3);
    P1OUT &= ~((1<<0)|(1<<3));
    P1IE = (1<<0) | (1<<3);
    P1IES &= ~((1<<0)|(1<<3));
}

void SetupRegisters(void)
{
    // reset registers //todo eruit gehaald
    P2DIR &= ~(1 << 0); // Input gemaakt Midden sensor
    P2DIR &= ~(1 << 1); // Input gemaakt links en rechts sensoren
    P2DIR &= ~(1 << 3);
    P2DIR &= ~(1 << 4); // 4e sensor voor hoeken
    P2DIR &= ~(1 << 6); //ultrasoon ingang

    P2SEL &= ~(1<<6); //ingang maken van P1.6 (speciale pin)

    P2IE |= (1 << 0);
    P2IE |= (1 << 1);
    P2IE |= (1 << 3);
    //P2IE |= (1 << 6);
    //P2IES |= ((1 << 0) | (1 << 3));
    P2IES &= ~(1 << 0);
    P2IES &= ~(1 << 1);
    P2IES &= ~(1 << 3);
    //P2IES &= ~(1 << 6);
    // end reset registers
}

void SetPWM(void)
{
    P2DIR |= (1 << 2) | (1 << 5); //PIN HOOG OUTPUT
    P2SEL |= (1 << 2) | (1 << 5); // OUTPUT CLOCK GAAT NAAR DE OUTPUT PIN

    if (CALBC1_1MHZ == 0xFF)
    {
        while (1)
            ; // Doe niets
    }

    /*
     * set clock frequentie on 1Mhz
     */
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    TA1CTL = TASSEL_2 | ID_3 | MC_1;

    TA1CCR0 = 300; // period time for pwm 1 and 2

    TA1CCTL1 = OUTMOD_7; // dutycycle pwm 1
    TA1CCR1 = 100;

    TA1CCTL2 = OUTMOD_7; // dutycycle pwm 2
    TA1CCR2 = 100;
}

float GemiddeldeAfstand(void)
{
    float kliks = (float)((klik+klik2)*0.5);
    return  (kliks*0.909);
}


int main(void)
{
    WDTCTL = WDTPW + WDTHOLD;                                   // Stop Watch Dog Timer
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;                                       // submainclock 1mhz

    SetupRegisters();

    SetPWM();

    //Encoder
    SetupEncoder();

    /* Init SPI P1.5, 1.6, 1.7 */
    SPI_begin();

    /* Init RC522 */
    RC522_init(RC522_CS_PIN, RC522_RST_PIN);


    /*
     * Setting antenna gain to maximum (7) can read cards up to approximately 5cm.
     * Default can read cards up to approximately 1cm.
     * */
    RC522_setAntennaGain(7);

    UART_setup();
    IE2 |= UCA0RXIE;

    CCTL0 |= CCIE;                                                                                  //Gerben, CCR0 interrupt enabled
    CCR0 = 1000;                                                                                  //Gerben, 1 seconden timer op 1 mhz
    TACTL |= TASSEL_2 | MC_1;                                                                       //Gerben, SMCLK aanzetten en MC op Stop

    __enable_interrupt();

    char wasOff = 1;

    while (1)
    {
        /*if((P2IN & 1<<6) == (1<<6) && start == 1)
        {
            Ultra = 1;
            stop = 1;
            Motor(Right, Off, PWMrechts);
            Motor(Left, On, PWMlinks);
            Ultra = 0;
            stop = 0;
        }*/



        if(start){
            if(wasOff){
                Motor(Right,On,PWMrechts);
                Motor(Left,On,PWMlinks);
                wasOff = 0;
            }
            Route_Afleggen();
            Afstand = GemiddeldeAfstand();
            UART_putc(GetDistance);
            UART_putc(Afstand);
        }
        else{
            wasOff = 1;
            Motor(Right,Off,0);
            Motor(Left,Off,0);
        }
        NFC_reader();
    }
#pragma diag_suppress = 112 // disable return warning
    return 0;
#pragma diag_default = 112
}

// hulp functie
void Motor(Side side, Action action, int pwm)
{
    if (pwm <= TA1CCR0) // check if the pwm value is lower then the max pwm value
    {
        switch (side) // select the engine
        {
        case Left:
            switch (action) // set on or off
            {
            case Off:
                TA1CCR1 = 0;
                break;
            case On:
                TA1CCR1 = pwm;
                break;
            }
            break;
        case Right:
            switch (action) // set on or off
            {
            case Off:
                TA1CCR2 = 0;
                break;
            case On:
                TA1CCR2 = pwm;
                break;
            }
            break;
        }
    }
}

void NFC_reader()
{
    /* Make space to store response */
    uint8_t str[MAX_LEN];
    /* See if there are cards present */
    if(RC522_request(PICC_REQALL, str) == MI_OK)
    {
        /* Can we communicate with the first detected card? */
        if(RC522_anticoll(str) == MI_OK)
        {
            /* Select card to be active */
            uint8_t l = RC522_selectTag(str);

            uint8_t keyA[6] = {0xBA, 0xBE, 0xBA, 0xBE, 0xBA, 0xBE};
            if(RC522_auth(PICC_AUTHENT1A, 1, keyA, str) == MI_OK)
            {
                if(RC522_readBlock(1, recvData) == MI_OK)
                {
                    NFCledkleur = recvData[0];
                    UART_putc(GetColor);
                    UART_putc(NFCledkleur);
                }
            }
            if(RC522_auth(PICC_AUTHENT1A, 9, keyA, str) == MI_OK)
            {
                if(RC522_readBlock(9, recvData) == MI_OK)
                {
                    if(recvData[6] > 127){
                        twoComplements = recvData[6]-1;
                        twoComplements ^= (0xFF);
                        twoComplements = -twoComplements;
                    }
                    else{
                        twoComplements = recvData[6];
                    }
                    UART_putc(GetTwoComplements);
                    UART_putc(twoComplements);
                    __delay_cycles(500000);
                }
            }
            /* Stop authentication */
            RC522_stopCrypto();
            RC522_halt();
            /* Put card into sleep mode */
        }

    }
    //return 0;
}
