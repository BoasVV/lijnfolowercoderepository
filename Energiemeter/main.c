#include <msp430.h>
#include "../Lib/i2c.h"
//#include "i2c2.h"
#include "../Lib/oled.h"
#include "../Lib/gpio.h"
#include "../Lib/fonts.h"
#include "../Lib/Itoa.c"

/**
 * main.c
 *
 * pinout energie meter
 *
 * P1.0 PP_CS_ADC10 -- current sense
 * P1.1 PP_VS_ADC10 -- voltage sense
 * P1.2 N/C
 * P1.3 N/C
 * P1.4 N/C
 * P1.5 N/C
 * P1.6 I2C_SCL -- I2C SCL
 * P1.7 I2C_SDA -- I2C SDA
 *
 * P2.0 EGB_CMOS_RST_IN -- reset signal
 * P2.1 EGB_CMOS_WT_IN -- show total (weergave totaal)
 * P2.2 EGB_CMOS_MA_OUT -- measuring active
 * P2.3 N/C
 * P2.4 N/C
 * P2.5 N/C
 * P2.6 XTAL_IN -- xtal in (16 Mhz)
 * P2.7 XTAL_OUT -- xtal out (16 Mhz)
 */

void Energiemeterscherm(void); //Roy, display aansturing voor op de energiemeter
void Energiebar(void);
void oledInitialize(void);                    //Roy, weergaven functie aanroepen
void oledClearScreen(void);                   //Roy, weergaven functie aanroepen
void oledSetArea(uint8_t, uint8_t, uint8_t, uint8_t);         //Roy, gebied oled
void oledFillBox(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);   //Roy, fill box

int paginaweergaven;                                //Roy, paginanummer variabel
int toggleknop = 0;
int startup = 0;
int scherm_update_energie = 0;                     //Roy, Scherm update variabel
int batterijbar;                                              //Roy, verbruikbar
int batterijverbruik;                            //Roy, verbruik van de batterij
int scherm_timer = 0;
int togglescherm_update = 0;
char * batterijverbruikweergeven[2];          //Roy, getal van verbruikweergaven
char * volt_weergaven[2]; //Roy, variabel om spanning weer te geven
char * volt_decimalen[2]; //Roy, variabel om de decimalen van de spanning weer te geven
char * stroom_weergaven[1];            //Roy, variabel om spanning weer te geven
char * stroom_decimalen[3]; //Roy, variabel om de decimalen van de stroom weer te geven
char * vermogen_weergaven[2];          //Roy, variabel om spanning weer te geven
char * vermogen_decimalen[2]; //Roy, variabel om de decimalen van de vermogen weer te geven
char * energie_weergaven[5];           //Roy, variabel om spanning weer te geven

char * displayMEM_V_weergaven[2];
char * displayMEM_V_decimalen[2];
char * displayMEM_A_weergaven[1];
char * displayMEM_A_decimalen[3];
char * displayP_weergaven[2];
char * displayP_decimalen[2];
char * displayE_weergaven[5];

static unsigned long E;
static unsigned long P;

static unsigned long displayP;
static unsigned int displayMEM_A;
static unsigned int displayMEM_V;
static unsigned int displayE;

static unsigned int meting1;
static unsigned int meting2;

static unsigned int VAR;
static unsigned int MEM_V;
static unsigned int MEM_A;
static unsigned int count = 0;

unsigned long Ecopy;





#pragma vector = TIMER0_A1_VECTOR
__interrupt void Riemannsom(void)
{
    scherm_timer++;
    ADC10CTL0 |= ADC10SC;
    TA0CTL &= ~(TAIFG);
}



#pragma vector = ADC10_VECTOR
__interrupt void ADCMEM(void)
{
                                                                                              // led aan tijdens de metinga

            count++;
            if (count == 1)
            {

               meting1 = ADC10MEM;
               VAR = ((meting1* 0.006378598) * 1000);

               if ((VAR >= 0) && (VAR <= 250)){

                   MEM_A = ((meting1 * 0.006400589)*1000);
               }

               if ((VAR > 250) && (VAR < 2400)){

                  MEM_A = ((meting1 * 0.006378589)* 1000);

               }

               if ((VAR >= 2400) && (VAR <= 10000)){

                  MEM_A = ((meting1 * 0.00612599)*1000);

               }


               ADC10CTL0 &= ~ENC;
               ADC10CTL1 = ADC10SSEL_3 | ADC10DIV_0 | SHS_0 | INCH_1 | CONSEQ_0;
               ADC10CTL0 |= ADC10SC| ENC;

            }

            if (count == 2)
            {
                meting2 = ADC10MEM;
                if ((VAR > 250) && (VAR < 2400)){

                MEM_V = ((meting2 * 0.0202999) * 1000);

                }
                if ((VAR >= 0) && (VAR <= 250)){

                  MEM_V = ((meting2 * 0.0203877) * 1000);

                }

                if ((VAR >= 2400) && (VAR <= 10000)){

                  MEM_V = ((meting2 * 0.0210081) * 1000);

                }

                P = ((((long)MEM_V) * ((long)MEM_A))/1000); //voorbeeld 10.20W 102000

                E += (P * 0.015); //0.6W 6000 = E = 96 , 62.5 keer sampelen voor joulz
                Ecopy = (E * 10);

                count = 0;
                ADC10CTL0 &= ~ENC;
                ADC10CTL1 = ADC10SSEL_3 | ADC10DIV_0 | SHS_0 | INCH_0 | CONSEQ_0;
                ADC10CTL0 |= ENC;


            }




}


int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timera
    __delay_cycles(1000000);

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ;
    BCSCTL2 = DIVS_0;                                                                            // 16 MHz SMCLK

    //VLO
    BCSCTL3 = LFXT1S_2;
    TA0CTL = TASSEL_1 | ID_2 | MC_1 | TAIE;
    TA0CCR0 = 50 -1 ;                                                                           //1.17s
    ///




    // interrupt KNOP
    P2DIR &= ~(BIT0 | BIT1);
    P2DIR |= BIT2;
    P2OUT &= ~BIT2;


    P2REN |= BIT0 | BIT1;
    P2OUT &= ~(BIT0 | BIT1);



    //// ADC10

    ADC10CTL0 = ADC10SHT_3 | SREF_1 | REFON | ADC10ON | REFBURST| ADC10IE | REF2_5V;
    ADC10CTL1 = ADC10SSEL_0 | ADC10DIV_1 | SHS_0 | INCH_0 | CONSEQ_0;
    ADC10CTL0 |= ENC;



    oledInitialize();                         //Roy, weergaven display instellen
    oledClearScreen();                         //Roy, weergaven display resetten
    oledSetArea(0, 0, 127, 7);
    oledSetOrientation(NORMAL);

    __enable_interrupt();



    while (1)
    {
        Energiemeterscherm();
        if (((P2IN & BIT0) !=0) && ((P2IN & BIT1) == 0) && (toggleknop == 0)){
            // moet RST alles op 0 tegelijk
            E=0;
            displayE = 0;
            displayP = 0;
            displayMEM_A = 0;
            displayMEM_V = 0;
            P2OUT &= ~BIT2;
            paginaweergaven = 5;
            toggleknop = 1;
            if(togglescherm_update == 1)
            {
                scherm_update_energie = 1;
                togglescherm_update = 0;
            }
        }

        else if (((P2IN & BIT0) ==0) && ((P2IN & BIT1) != 0) && (toggleknop == 0))
        {
                    // moet STOP lees display var kan ook te gelijk weergeven
            displayE = E;
            displayP = P;
            displayMEM_A = MEM_A;
            displayMEM_V = MEM_V;
            P2OUT &= ~BIT2;
            paginaweergaven = 6;
            toggleknop = 1;
            if(togglescherm_update == 1)
            {
                scherm_update_energie = 1;
                togglescherm_update = 0;
            }
        }

        if (((P2IN & BIT1) == 0) && ((P2IN & BIT0) == 0))
        {
            //verderme while
            P2OUT |= BIT2;
            togglescherm_update = 1;
            toggleknop = 0;

            if(startup == 0)
            {
                startup = 1;
                paginaweergaven = 0;
                scherm_update_energie = 1;
            }
            if ((scherm_timer >= 100) && ((paginaweergaven != 5) || (paginaweergaven != 6)))
            {
                paginaweergaven++;
                scherm_timer = 0;
                scherm_update_energie = 1;
                if (paginaweergaven == 4)
                {
                    paginaweergaven = 0;
                }
            }
            if(((paginaweergaven == 5) || (paginaweergaven == 6)) || ((paginaweergaven != 1) && (paginaweergaven != 2) && (paginaweergaven != 3) && (paginaweergaven != 5) && (paginaweergaven != 6)))
            {
                paginaweergaven = 0;
            }
        }
    }
}

void Energiemeterscherm()    //Roy, hoofd energiemeter weergaven aansturing code
{
    if (scherm_update_energie == 1)                         //scherm mag updaten
    {
        oledClearScreen();                                    //scherm verversen
        int arrcounter;
        switch (paginaweergaven)
        {
        case 0:
            oledPrint(0, 0, "Spanning", big);
            itoa(MEM_V / 1000, volt_weergaven, 2);                    //volt omzetten
            itoa((MEM_V % 1000) * 0.1, volt_decimalen, 2);               //decimalen omzetten
            oledPrint(40, 3, volt_weergaven, big);              //volt weergeven
            oledPrint(60, 3, ".", big);
            oledPrint(80, 3, volt_decimalen, big);         //decimalen weergeven
            oledPrint(110, 3, "V", big);
            break;
        case 1:
            oledPrint(0, 0, "Stroom", big);
            itoa(MEM_A /1000 , stroom_weergaven, 1);               //stroom omzetten
            itoa(((MEM_A % 1000)), stroom_decimalen, 3);         //decimalen omzetten
            oledPrint(40, 3, stroom_weergaven, big);           //stroom weergeven
            oledPrint(60, 3, ".", big);
            oledPrint(80, 3, stroom_decimalen, big);       //decimalen weergeven
            oledPrint(110, 3, "A", big);
            break;
        case 2:
            oledPrint(0, 0, "Vermogen", big);
            ltoa(P / 1000, vermogen_weergaven, 2);         //vermogen omzetten
            ltoa((P % 1000) / 10, vermogen_decimalen, 2);     //decimalen omzetten
            oledPrint(40, 3, vermogen_weergaven, big);       //vermogen weergeven
            oledPrint(60, 3, ".", big);
            oledPrint(80, 3, vermogen_decimalen, big);     //decimalen weergeven
            oledPrint(110, 3, "W", big);
            break;
        case 3:
            oledPrint(0, 0, "Energie", big);
            if((Ecopy/10000) < 99999)
            {
                ltoa(Ecopy / 10000 , energie_weergaven, 0);            //energie omzetten
                oledPrint(20, 3, energie_weergaven, big);         //energie weergevena
            }
            else
            {
                ltoa(99999, energie_weergaven, 0);            //energie omzetten
                oledPrint(20, 3, energie_weergaven, big);         //energie weergevena
            }
            oledPrint(110, 3, "J", big);
            break;
        case 5:
            E=0;
            oledPrint(0, 0, "Reset", big);
            oledPrint(0, 4, "Spanning: 0.0 V", small);
            oledPrint(0, 5, "Stroom:   0.0 A", small);
            oledPrint(0, 6, "Vermogen: 0.0 W", small);
            oledPrint(0, 7, "Energie:  0.0 J", small);       //vermogen weergeven
            break;
        case 6:
            itoa(displayMEM_V / 1000, displayMEM_V_weergaven, 2);         //vermogen omzetten
            itoa(((displayMEM_V % 1000) / 10), displayMEM_V_decimalen, 2);     //decimalen omzetten

            itoa(displayMEM_A / 1000, displayMEM_A_weergaven, 1);         //vermogen omzetten
            itoa(displayMEM_A % 1000, displayMEM_A_decimalen, 3);     //decimalen omzetten

            ltoa(displayP / 1000, displayP_weergaven, 2);         //vermogen omzetten
            ltoa(((displayP % 1000) / 10), displayP_decimalen, 2);     //decimalen omzetten

            ltoa(Ecopy / 10000, displayE_weergaven, 0);         //vermogen omzetten

            oledPrint(0, 4, "Spanning:", small);
            oledPrint(60, 4, displayMEM_V_weergaven, small);       //vermogen weergeven
            oledPrint(70, 4, ".", small);
            oledPrint(90, 4, displayMEM_V_decimalen, small);     //decimalen weergeven
            oledPrint(120, 4, "V", small);

            oledPrint(0, 5, "Stroom:", small);
            oledPrint(60, 5, displayMEM_A_weergaven, small);       //vermogen weergeven
            oledPrint(70, 5, ".", small);
            oledPrint(90, 5, displayMEM_A_decimalen, small);     //decimalen weergeven
            oledPrint(120, 5, "A", small);

            oledPrint(0, 6, "Vermogen:", small);
            oledPrint(60, 6, displayP_weergaven, small);       //vermogen weergeven
            oledPrint(70, 6, ".", small);
            oledPrint(90, 6, displayP_decimalen, small);     //decimalen weergeven
            oledPrint(120, 6, "W", small);

            oledPrint(0, 7, "Energie:", small);
            oledPrint(70, 7, displayE_weergaven, small);       //vermogen weergeven
            oledPrint(120, 7, "J", small);
            break;
        default:
            oledPrint(0, 0, "Error", big);
            break;
        }
        if((paginaweergaven == 0) || (paginaweergaven == 1) || (paginaweergaven == 2) || (paginaweergaven == 3))
        {
            scherm_update_energie = 2;                             //naar energiebar
            Energiebar();
        }
        if((paginaweergaven == 5) || (paginaweergaven == 6))
        {
            scherm_update_energie = 0;
        }
    }
}

void Energiebar()
{
    if (scherm_update_energie == 2)
    {
        batterijverbruik = (MEM_V / 180);
        batterijbar = (batterijverbruik + 13); //batterijbar op goede positie zetten
        itoa(batterijverbruik, batterijverbruikweergeven, 3); //batterij procent omzettena
        oledPrint(95, 6, batterijverbruikweergeven, small);
        oledPrint(120, 6, "%", small);
        oledFillBox(13, 7, batterijbar, 7, 0xFF);          //batterijbar printen
        scherm_update_energie = 0;
    }
}
