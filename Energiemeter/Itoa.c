/*
 * temperatuur.c
 *
 *  Created on: 12 mrt. 2018
 *      Author: VersD
 *  Revised on: 11 jun. 2019
 *      Author: DuijndamR
 */

#include <stdint.h>

/* itoa:  integer naar ascii omzetting */
void itoa(int n, char s[], int l)
{
    int i, sign ;

    // als negatief
    if ((sign = n) < 0)
    {
        n = -n; //maak positief
    }

    i = 0;

    do
    {
        s[i++] = n % 10 + '0'; // Offset vanaf '0' character
    }
    while((n /= 10) > 0); // Delen door 10 en opnieuw a

    while(i < l){
        s[i++] = '0';
    }

    // Als negatief
    if(sign < 0)
    {
        s[i++] = '-'; // Voeg - toe
    }

    s[i] = '\0'; // Eindigen string


    reverse(s); // Draai de boel om
}

void ltoa(unsigned long n, char s[], int l)
{
    int i;
    unsigned long sign ;

    // als negatief
    if ((sign = n) < 0)
    {
        n = -n; //maak positief
    }

    i = 0;

    do
    {
        s[i++] = n % 10 + '0'; // Offset vanaf '0' character
    }
    while((n /= 10) > 0); // Delen door 10 en opnieuw a

    while(i < l){
        s[i++] = '0';
    }

    // Als negatief
    if(sign < 0)
    {
        s[i++] = '-'; // Voeg - toe
    }

    s[i] = '\0'; // Eindigen string


    reverse(s); // Draai de boel om
}
