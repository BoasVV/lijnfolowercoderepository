
#ifndef LIB_OLED_H_
#define LIB_OLED_H_

#include "fonts.h"
/**
 * \defgroup OLED
 * @brief Aantal functies om de oleddisplay te gebruiken
 *
 * De oleddisplay werkt op basis van de SSD1306
 * https://cdn-shop.adafruit.com/datasheets/SSD1306.pdf
 *
 * Dit is een I2C-module. Het display is opgebouwd uit
 * 8 pages (verticaal) en 128 kolommen (horizontaal)
 *
 * Elke page bestaat verticaal uit 8 pixels. Deze 8 pixels
 * moeten tegelijk worden beschreven. Het is dus niet mogelijk
 * om individuele pixels aan te zetten zonder het gebruik
 * van een framebuffer.
 *
 * Deze lib heeft een kleine framebuffer ter grootte van 2 pages
 * waarin dit wel mogelijk is.
 *
 * \todo Nog niet alle schermfunctionaliteiten hebben een functie gekregen
 * @{
 *
 */


//0x78 = 0b1111000
#define OLED_ADR (0x3C)

//oled spul
// Control byte
#define OLED_CONTROL_BYTE_CMD_SINGLE    0x80
#define OLED_CONTROL_BYTE_CMD_STREAM    0x00
#define OLED_CONTROL_BYTE_DATA_STREAM   0x40

// Fundamental commands (pg.28)
#define OLED_CMD_SET_CONTRAST           0x81    // follow with 0x7F
#define OLED_CMD_DISPLAY_RAM            0xA4
#define OLED_CMD_DISPLAY_ALLON          0xA5
#define OLED_CMD_DISPLAY_NORMAL         0xA6
#define OLED_CMD_DISPLAY_INVERTED       0xA7
#define OLED_CMD_DISPLAY_OFF            0xAE
#define OLED_CMD_DISPLAY_ON             0xAF

// Addressing Command Table (pg.30)
#define OLED_CMD_SET_MEMORY_ADDR_MODE   0x20    // follow with 0x00 = HORZ mode = Behave like a KS108 graphic LCD
#define OLED_CMD_SET_COLUMN_RANGE       0x21    // can be used only in HORZ/VERT mode - follow with 0x00 and 0x7F = COL127
#define OLED_CMD_SET_PAGE_RANGE         0x22    // can be used only in HORZ/VERT mode - follow with 0x00 and 0x07 = PAGE7

// Hardware Config (pg.31)
#define OLED_CMD_SET_DISPLAY_START_LINE 0x40
#define OLED_CMD_SET_SEGMENT_REMAP      0xA1
#define OLED_CMD_CLR_SEGMENT_REMAP      0xA0
#define OLED_CMD_SET_MUX_RATIO          0xA8    // follow with 0x3F = 64 MUX
#define OLED_CMD_SET_COM_SCAN_MODE      0xC8
#define OLED_CMD_CLR_COM_SCAN_MODE      0xC0
#define OLED_CMD_SET_DISPLAY_OFFSET     0xD3    // follow with 0x00
#define OLED_CMD_SET_COM_PIN_MAP        0xDA    // follow with 0x12
#define OLED_CMD_NOP                    0xE3    // NOP

// Timing and Driving Scheme (pg.32)
#define OLED_CMD_SET_DISPLAY_CLK_DIV    0xD5    // follow with 0x80
#define OLED_CMD_SET_PRECHARGE          0xD9    // follow with 0xF1
#define OLED_CMD_SET_VCOMH_DESELCT      0xDB    // follow with 0x30

// Charge Pump (pg.62)
#define OLED_CMD_SET_CHARGE_PUMP        0x8D    // follow with 0x14

// Scrolling #defines
#define OLED_CMD_SCROLL_ACTIVATE                0x2F
#define OLED_CMD_SCROLL_DEACTIVATE              0x2E
#define OLED_CMD_SCROLL_SET_VERTICAL_AREA       0xA3
#define OLED_CMD_SCROLL_RIGHT_HORIZONTAL        0x26
#define OLED_CMD_SCROLL_LEFT_HORIZONTAL         0x27
#define OLED_CMD_SCROLL_VERTICAL_AND_RIGHT_HORIZONTAL   0x29
#define OLED_CMD_SCROLL_VERTICAL_AND_LEFT_HORIZONTAL    0x2A


/*!
 * Mogelijke rotaties van het scherm.
 */
typedef enum {NORMAL, FLIPPED} Rotation;

/*!
 * @brief Dit initialiseert P1.6 en P1.7 naar I2C en bereidt het display voor
 */
void oledInitialize(void );

/*!
 * @brief Dit stelt een rechthoek in waarbinnen de pixels worden aangepast
 *
 * Als je bijvoorbeeld een rechthoek van (0,0) tot (2,10) selecteert,
 * dan kun je hierna 2*10 bytes sturen om de 8*2*10 pixels op te vullen.
 * Volgorde is vanaf de bovenste page, eerst de page opvullen van links naar rechts
 * vervolgens een volgende page van links naar rechts.
 *
 * @code
 * oledSetArea(0, 0, 2, 10);
 * i2cStart();
 * i2cSendByte(OLED_CONTROL_BYTE_DATA_STREAM);
 * for (int kolom = 0; kolom < 10; kolom++)
 * {
 *      for (int rij = 0; rij < 2; rij++)
 *      {
 *          i2cSendByte(...);
 *      }
 * }
 * i2cStop();
 * @endcode
 *
 * \param startPage Bovenste page om mee te beginnen
 * \param stopPage Onderste page om bij te stoppen
 * \param startKolom Linker kolom
 * \param stopKolom Rechter kolom
 */
void oledSetArea(uint8_t startKolom, uint8_t startPage, uint8_t stopKolom, uint8_t stopPage);

/*!
 * @brief Hiermee kun je het display draaien. Handig  voor een breadboard.
 *
 * Let op dat het bestaande display niet wordt omgedraaid. Het heeft
 * slechts invloed op toekomstige tekenopdrachten.
 *
 * \param orientation Flipped of Normal
 */
void oledSetOrientation(Rotation orientation);

/*!
 * @brief Vul een box met dezelfde waarde voor elke kolom
 *
 * Let hierbij op dat het LSB->MSB van boven->beneden gaat.
 *
 * @code
 * oledFillBox(0, 0, 127, 7, 0xAA); // Allemaal lijnen!
 * @endcode
 *
 */
void oledFillBox(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t waarde);

/*!
 * @brief Maakt de hele box zwart @see oledFillBox
 */
void oledClearBox(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);

/*!
 * @brief Maakt het scherm zwart @see oledClearBox
 */
void oledClearScreen();

/*!
 * @brief Dit tekent een karakter met een bepaalde grootte
 *
 *
 * \param page Op welke page komt het karakter
 * \param column Op welke kolom begint het karakter
 * \param c Het te printen karakter
 * \param grootte De gekozen font-size
 * \return Geeft de laatst gebruikte kolom terug.
 */
uint8_t oledDrawFontChar(uint8_t page, uint8_t column, char c, Font grootte);

/*!
 * @brief Dit tekent een string met een bepaalde grootte @see oledDrawFontChar
 *
 * \param page Op welke page komt de string
 * \param column Op welke kolom begint de string
 * \param c De zin om te printen
 * \param grootte De gekozen font-size
 * \return Geeft de laatste geprinte kolom terug.
 */
uint8_t oledPrint(uint8_t column, uint8_t page, char *c, Font grootte);

/*!
 * @brief Schrijf de pageBuffer naar een bepaalde page
 *
 * \param page Op welke page wordt de buffer geplaatst
 * \param npages Hoeveel pages gaat het om, 1 of 2?
 */
void oledWriteBuffer(uint8_t page, uint8_t npages);

/*!
 * @brief Zet een pixel aan op de x, y locatie.
 *
 * Let op, is anders geori�nteerd dan het display zelf.
 *
 * Co�rdinaat (0, 0) is links onder, (15, 127) is rechtsboven.
 *
 * \param x x-co�rdinaat van 0 tot 127
 * \param y y-co�rdinaat van 0 tot 15
 */
void oledSetBufferPixel(uint8_t x, uint8_t y);

/*!
 * @brief Zet een pixel uit op de x, y locatie. @see oledSetBufferPixel
 */
void oledClearBufferPixel(uint8_t x, uint8_t y);


#endif /* LIB_OLED_H_ */
