/*
 * temperatuur.c
 *
 *  Created on: 12 mrt. 2018
 *      Author: VersD
 *  Revised on: 11 jun. 2019
 *      Author: DuijndamR
 */

#include <stdint.h>
#include <string.h>

/* Aantal hulp functies voor getallen en tekst */
/* Draai array om */
static void reverse(char s[])
{
    int i, j ;
    char c ;

    // Tegelijk optellen en aftellen
    for (i = 0, j = strlen(s) - 1; i < j ; i++, j-- )
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* itoa:  integer naar ascii omzetting */
void itoa(int n, char s[],int l) // l wordt niet gebruikt
{
    int i, sign ;

    // als negatief
    if ((sign = n) < 0)
    {
        n = -n; //maak positief
    }

    i = 0;

    do
    {
        s[i++] = n % 10 + '0'; // Offset vanaf '0' character
    }
    while((n /= 10) > 0); // Delen door 10 en opnieuw

    // Als negatief
    if(sign < 0)
    {
        s[i++] = '-'; // Voeg - toe
    }

    s[i] = '\0'; // Eindigen string

    reverse(s); // Draai de boel om
}
