/*
 * Itoa.h
 *
 *  Created on: 12 Jun 2019
 *      Author: model
 */

#ifndef ITOA_H_
#define ITOA_H_

#include <stdint.h>

void itoa(int n, char s[],int l);


#endif /* ITOA_H_ */
