#include "Itoa.h"
#include <msp430.h>
#include <stdint.h>
#include <string.h>
#include "Uart.h"
#include "../Lib/gpio.h"
//#include "../Lib/i2c.h"
#include "../Lib/oled.h"
#include "../Lib/fonts.h"
#include "i2c2.h"

/**
 * main.c
 *
 * pinout cpu 2
 *
 * P1.0 LedR -- RGB led Red
 * P1.1 LedG -- uart
 * P1.2 LedB -- uart
 * P1.3 Knipper1 -- Richtings aanwijzer 1
 * P1.4 Knipper2 -- Richtings aanwijzer 2
 * P1.5 Knipper3 -- Richtings aanwijzer 3
 * P1.6 I2C_SCL -- I2C SCL
 * P1.7 I2C_SDA -- I2C SDA
 *
 * P2.0 EGB_CMOS_RST_OUT -- energie meter reset
 * P2.1 EGB_CMOS_WT_OUT -- energie meter weergave totaal
 * P2.2 EGB_CMOS_MA_IN -- energie meter meting actief
 * P2.3 Button1 -- button 1 menu
 * P2.4 Button2 -- button 2 menu
 * P2.5 Button3 -- button 3 menu
 * P2.6 XTAL_IN -- RBG led Green
 * P2.7 XTAL_OUT -- RGB led Blue
 */

uint8_t *recv;
int messcount = 0;
actions action = NONE;

int afstand = 0;                                                                                    //Boas, Afstand int
int twocomplement = 0;                                                                              //Boas, two complement int
int color;                                                                                          //Boas, readed color
int done = 0;                                                                                       //Boas, if second msp is busy this value is 1 otherwise it is 0
int direction;//knipper param
int scherm_update = 0;                                                                              //Roy, scherm updaten

#pragma vector = USCIAB0RX_VECTOR
__interrupt void UART_RX_Interrupt(){
    if(IFG2 & UCA0RXIFG){                                                                           //Boas, check if the interrupt flag is from the receive buffer
        if(action != NONE){                                                                         //Boas, only read data if action is pressent
            switch(action){                                                                         //Boas, switches thru the actions
            case GetDistance:
                afstand = UCA0RXBUF;                                                                //Boas, gets the driven value
                scherm_update=1;
                break;
            case GetTwoComplements:
                twocomplement += UCA0RXBUF;                                                          //Boas, gets the readed two complements value
                scherm_update=1;
                break;
            case GetNextDirection:
                direction = UCA0RXBUF;                                                                    //Boas, gets the new direction
                break;
            case GetColor:
                color = UCA0RXBUF;                                                                  //Boas, gets the readed color
                NFC_leds(color);                                                                    //Boas, Sets the NFC led
                break;
            case GetDone:
                done = UCA0RXBUF;                                                                   //Boas, gets the status value
                break;
            }
            action = NONE;                                                                          //Boas, if action has ended reset the action
        }
        else{
            action = UCA0RXBUF;                                                                     //Boas, receive the action
        }
    }
}

static int pn = 100;                                                                                //Roy, weergaven paginanummer static nummer
static int ski;                                                                                     //Roy, start knop ingedrukt
int startscherm = 0;                                                                                //Roy, weergaven opstartscherm defnitie
int motorsrunning = 0;                                                                              //Roy, weergaven richtinsprogramma runt
int invoerenroute;                                                                                  //Roy, invoerenroute actief
unsigned int r;                                                                                     //Roy, for loop
unsigned char Route[42];                                                                            //Roy, route
char getal[10];                                                                                     //Roy, array voor printen aanmaken
int wachten_op_start;                                                                               //Roy, route is ingevuld
int seconden = 0;                                                                                   //Gerben, Integer seconden 0 maken.
int view_seconden = 0;                                                                              //Roy, seconden naar een string zetten
int knipper;
long divideseconden;
int stoptellen;
int starttellen;
void opstartscherm(void);                                                                           //Roy bij aanzetten functie
void oledInitialize(void);                                                                          //Roy, weergaven functie aanroepen
void oledClearScreen(void);                                                                         //Roy, weergaven functie aanroepen
void oledSetArea(uint8_t, uint8_t, uint8_t, uint8_t);                                               //Roy, gebied oled
void oledFillBox(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);                                      //Roy, fill box
void weergaven_robot(void);                                                                         //Roy, weergaven functie aanroepen voor display printen
void richtingscode(void);                                                                           //Roy, weergaven functie aanroepen voor richtingscode
void Start_Timer(int);                                                                              //Gerben, start timer
void NFC_leds(unsigned char NFC_kleur);                                                             //Roy, NFC ledindicatie
void knipperlicht(void);                                                                            //Roy, knipperlicht indicatie

int millis = 0;

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer_A(void)
{
    millis++;
    if(millis >= 1000){
        if (seconden <= 99999)                                                                          // als seconden lager is of gelijk aan de maximale waarde dan mag hij de statement in.
        {
            seconden++;                                                                                 // tel er een seconde op.
            divideseconden = (seconden*2);
        }
        millis=0;
    }
}

#pragma vector = PORT2_VECTOR
__interrupt void Knoppen(void)
{
    if (((P2IN & 1 << 4) == 1 << 4) && (pn != 5) && (pn != 7) && (pn != 11)
            && (pn != 20) && (pn != 30) && (pn != 31))                                                                          //als pin 2.4 hoog is / boven kant van elk menu defineren
    {
        pn++;                                                                                       //paginanummer omhoog
        scherm_update = 1;                                                                          //scherm mag updaten
        P2IFG &= ~(1 << 4);
    }
    if (((P2IN & 1 << 5) == 1 << 5) && (pn != 0) && (pn != 6) && (pn != 8)
            && (pn != 20) && (pn != 30) && (pn != 31))                                                                          //als pin 2.5 hoog is / de onderkant van elk menu defineren
    {
        pn--;                                                                                       //paginanummer omlaag
        scherm_update = 1;                                                                          //scherm mag updaten
        P2IFG &= ~(1 << 5);
    }
    if ((P2IN & 1 << 3) == (1 << 3))                                                                //als pin 2.3 hoog is
    {

        ski = 1;                                                                                    //start knop ingedrukt
        scherm_update = 1;                                                                          //scherm mag updaten
        P2IFG &= ~(1 << 3);
    }
    else
    {
        P2IFG &= ~(1 << 3 | 1 << 4 | 1 << 5);
        scherm_update = 1;                                                                          //scherm mag updaten
    }
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;                                                                       // stop watchdog timer
    __delay_cycles(1100000);                                                                        // Boas, Delay for display startup

    if (CALBC1_1MHZ == 0xFF)
    {
        while (1)
            ; // Doe niets
    }

    /*
     * set clock frequentie on 1Mhz
     */
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    P2DIR &= ~(1 << 3 | 1 << 4 | 1 << 5);                                                           //Roy, weergaven knoppen directen
    P2OUT &= ~(1 << 3 | 1 << 4 | 1 << 5);                                                           //Roy, weergaven knoppen laag zetten
    P2REN |=  (1 << 3 | 1 << 4 | 1 << 5);                                                           //Roy, weergaven pull down weerstanden instellen
    P2IES &= ~(1 << 3 | 1 << 4 | 1 << 5);                                                           //Roy, weergaven falling edge instellen.
    P2IE  |=  (1 << 3 | 1 << 4 | 1 << 5);                                                           //Roy, weergaven interrupt enable instellen

    P1DIR |=  (1 << 0);                                                                             //Roy, NFC leds bit 0 is rood
    P2DIR |=  (1 << 6 | 1 << 7);                                                                    //Roy, NFC leds bit 6 is groen bit 7 is blauw
    P1OUT &= ~(1 << 0);                                                                             //Roy, NFC leds laag
    P2OUT &= ~(1 << 6 | 1 << 7);

    P2SEL &= ~(1 << 6 | 1 << 7);                                                                    //Boas, NFC leds special pin uit zetten
    P2SEL2 &= ~(1 << 6 | 1 << 7);                                                                   //Boas, NFC leds special pin uit zetten

    P1DIR |=  (1 << 3 | 1 << 4 | 1 << 5);                                                           //Roy, knipperlicht, pin 3 is links pin 4 is rechts pin 5 is rechtdoor
    P1OUT &= ~(1 << 3 | 1 << 4 | 1 << 5);                                                           //Roy, knipperlicht uitgangen laag

    oledInitialize();                                                                               //Roy, weergaven display instellen
    oledClearScreen();                                                                              //Roy, weergaven display resetten
    oledSetArea(0, 0, 127, 7);                                                                      //Roy, weergaven resolutie instellen

    CCTL0 |= CCIE;                                                                                  //Gerben, CCR0 interrupt enabled
    CCR0 = 1000;                                                                                  //Gerben, 1 seconden timer op 1 mhz
    TACTL |= TASSEL_2 | MC_0;                                                                       //Gerben, SMCLK aanzetten en MC op Stop

    P1IFG = 0x00;                                                                                   //Gerben, Alle interupt flags 0 maken

    UART_setup();                                                                                   //Boas, UART setup
    IE2 |= UCA0RXIE;                                                                                //Boas, enable interrupt UART receive

    _BIS_SR(GIE);                                                                                   //Gerben, globale interrupt enable setten

    while (1)
    {
        opstartscherm();                                                                            //Roy, bij scherm opstarten
        weergaven_robot();                                                                          //Roy, scroll menu
        richtingscode();                                                                            //Roy, route richtingscode
        knipperlicht();                                                                             //Roy, knipperlichten

        if((done == 0) && (pn == 30))
        {
            Start_Timer(0);
            P1OUT |= (1 << 0);
            P2OUT |= (1 << 6 | 1 << 7);
            pn = 31;
            scherm_update = 1;
        }
        if(done == 1)
        {
            if(pn != 30){                                                                           //Boas, prevent update every time
                pn = 30;
                scherm_update=1;
            }
        }
    }
}

void Start_Timer(int state)
{
    if (state == 1)
    {
        TACTL |= MC_1;                                                                              // klok aanzetten
        //todo Clock doesnt start if operating without debugger attached, with debugger it works fine
    }
    if (state == 0)
    {
        TACTL &= ~MC_1;                                                                             // klok uitzetten
    }
}

void opstartscherm()
{
    if (startscherm == 0)                                                                           //Bij opstarten gaat dit scherm aan.
    {
        oledPrint(20, 3, "Press start", big);
    }
    if ((startscherm == 0) && (ski == 1))                                                           //uit start scherm gaan.
    {
        startscherm = 1;
        ski = 0;
        oledClearScreen();
        pn = 0;
    }
}

void weergaven_robot()                                                                               //Roy, weergaven display
{
    if (scherm_update == 1)                                                                         //gaat deze functie in als scherm mag updaten
    {
        oledClearScreen();                                                                          //ververst scherm
        switch (pn)
        {
        case 0:
            oledPrint(100, 3, "<--", big);
            oledPrint(25, 3, "Route", big);                                                         //vervang de text voor tablad text
            oledPrint(0, 7, "Rapport", small);
            break;
        case 1:
            oledPrint(100, 3, "<--", big);
            oledPrint(0, 0, "Route", small);
            oledPrint(25, 3, "Rapport", big);                                                       //vervang de text voor tablad text
            oledPrint(0, 7, "text3", small);
            break;
        case 2:
            oledPrint(100, 3, "<--", big);
            oledPrint(0, 0, "Rapport", small);
            oledPrint(25, 3, "text3", big);                                                         //vervang de text voor tablad text
            oledPrint(0, 7, "text4", small);
            break;
        case 3:
            oledPrint(100, 3, "<--", big);
            oledPrint(0, 0, "text3", small);
            oledPrint(25, 3, "text4", big);                                                         //vervang de text voor tablad text
            oledPrint(0, 7, "stop", small);
            break;
        case 4:
            oledPrint(100, 3, "<--", big);
            oledPrint(0, 0, "text4", small);
            oledPrint(25, 3, "stop", big);                                                         //vervang de text voor tablad text
            oledPrint(0, 7, "text6", small);
            break;
        case 5:
            oledPrint(100, 3, "<--", big);
            oledPrint(0, 0, "stop", small);
            oledPrint(25, 3, "text6", big);                                                         //vervang de text voor tablad text
            break;
        case 6:
            oledPrint(25, 3, "Invoeren", big);
            oledPrint(0, 7, "Terug", small);
            break;
        case 7:
            oledPrint(0, 0, "Invoeren", small);
            oledPrint(25, 3, "Terug", big);
            break;
        case 20:
            itoa(seconden, getal,0); // 0 wordt niet gebruikt                                       //Roy, seconden invoeren en de array getal eruit krijgen/ omzetting
            oledPrint(0, 2, "Tijd:", small);
            oledPrint(100, 2, getal, small);                                                        //Roy, seconden printen
            itoa(afstand, getal,0);
            oledPrint(0, 3, "Afstand:", small);
            oledPrint(100, 3, getal, small);
            itoa(twocomplement,getal,0);
            oledPrint(0, 4, "Two compliment:", small);
            oledPrint(100, 4, getal, small);
            oledPrint(0, 7, "Klik start voor terug", small);
            break;
        case 30:
            itoa(seconden, getal,0); // 0 wordt niet gebruikt                                       //Roy, seconden invoeren en de array getal eruit krijgen/ omzetting
            oledPrint(0, 1, "Tijd:", small);
            oledPrint(100, 1, getal, small);

            oledPrint(25, 3, "Busy", big);

            itoa(afstand,getal,0);
            oledPrint(0,7,"Afstand:",small);
            oledPrint(100,7,getal,small);

            itoa(twocomplement,getal,0);
            oledPrint(0, 6, "Two compliment:", small);
            oledPrint(100, 6, getal, small);

            break;
        case 31:
            oledPrint(25, 0, "Done", big);
            oledPrint(25, 3, "Press start", big);
            break;
        default:
            oledPrint(0, 0, ".", small);
            break;
        }
        scherm_update = 0;
        if ((ski == 1) && (scherm_update != 1) && (pn == 0))                                        //start knop wordt ingedrukt met selectie scherm
        {
            scherm_update = 1;
            ski = 0;                                                                                //start knop is niet meer ingedrukt
            pn = 6;                                                                                 //naar het richtingscode menu
        }
        if ((ski == 1) && (scherm_update != 1) && (pn == 1))                                        //start knop wordt ingedrukt met selectie scherm
        {
            scherm_update = 1;
            ski = 0;                                                                                //start knop is niet meer ingedrukt
            pn = 20;                                                                                //Rapportage menu

            //todo tijd
        }
        if ((ski == 1) && (scherm_update != 1) && (pn == 2))                                        //start knop wordt ingedrukt met selectie scherm
        {
            ski = 0;
            knipper = 1;                                                                        //todo tijdelijk
            starttellen = seconden;
        }
        if ((ski == 1) && (scherm_update != 1) && (pn == 3))                                        //start knop wordt ingedrukt met selectie scherm
        {
            ski = 0;
            knipper = 2;
            starttellen = seconden;
        }
        if ((ski == 1) && (scherm_update != 1) && (pn == 4))                                        //start knop wordt ingedrukt met selectie scherm
        {
            ski = 0;
            scherm_update = 1;
            UART_putc(Stop);                                                                        //Boas, Send to the second msp that it must stop
            UART_putc(Stop);
        }
        if ((ski == 1) && (scherm_update != 1) && (pn == 6))                                        //start knop wordt ingedrukt met selectie scherm
        {
            ski = 0;                                                                                //start knop is niet meer ingedrukt
            scherm_update = 1;
            invoerenroute = 1;                                                                      //naar invoer menu
            pn = 8;                                                                                 //naar scroll menu
        }
        if ((ski == 1) && (scherm_update != 1) && (pn == 7))                                        //start knop wordt ingedrukt met selectie scherm
        {
            ski = 0;                                                                                //start knop is niet meer ingedrukt
            scherm_update = 1;
            pn = 0;                                                                                 //terug naar begin scherm
        }
        if ((ski == 1) && (scherm_update != 1) && (pn == 20))                                       //start knop wordt ingedrukt met selectie scherm
        {
            ski = 0;                                                                                //start knop is niet meer ingedrukt
            scherm_update = 1;
            pn = 0;                                                                                 //naar scroll menu
        }
        if ((ski == 1) && (scherm_update != 1) && (pn == 30))                                       //start knop wordt ingedrukt met selectie scherm
        {
            ski = 0;                                                                                //start knop is niet meer ingedrukt
            scherm_update = 1;
            UART_putc(Stop);                                                                        //Boas, sends to the second msp that is must stop
            UART_putc(Stop);
        }
        if ((ski == 1) && (scherm_update != 1) && (pn == 31))                                        //start knop wordt ingedrukt met selectie scherm
        {
            ski = 0;
            pn = 0;
            scherm_update = 1;
        }
    }
}

void richtingscode()                                                                                //Roy, weergaven richtingscode
{
    if (invoerenroute == 1)                                                                         //invoer menu
    {
        for (r = 0; r < 42;)                                                                        //42 route selecties
        {
            char actienummer[10];
            itoa(r, actienummer,0); // 0 wordt niet gebruikt                                        //de waarde r omzetten naar een array om te printen
            if (pn == 8)                                                                            //optie 1 scherm
            {
                if (scherm_update == 1)
                {
                    oledClearScreen();
                    oledPrint(110, 0, actienummer, big);                                            //Actienummer rechtsboven weergeven
                    oledPrint(100, 3, "<--", big);
                    oledPrint(25, 3, "Links", big);
                    oledPrint(0, 7, "Rechts", small);
                    scherm_update = 0;
                }
                if ((ski == 1) && (scherm_update != 1))
                {
                    ski = 0;                                                                        //start knop is niet meer ingedrukt
                    scherm_update = 1;
                    Route[r] = 'L';                                                                 //Links in array zetten op r
                    r++;
                }
            }

            if (pn == 9)                                                                            //optie 2 scherm
            {
                if (scherm_update == 1)
                {
                    oledClearScreen();
                    oledPrint(110, 0, actienummer, big);                                            //Actienummer rechtsboven weergeven
                    oledPrint(100, 3, "<--", big);
                    oledPrint(0, 0, "Links", small);
                    oledPrint(25, 3, "Rechts", big);
                    oledPrint(0, 7, "Vooruit", small);
                    scherm_update = 0;
                }
                if ((ski == 1) && (scherm_update != 1))
                {
                    ski = 0;                                                                        //start knop is niet meer ingedrukt
                    scherm_update = 1;
                    Route[r] = 'R';                                                                 //rechts in array zetten op r
                    r++;
                }
            }

            if (pn == 10)
            {
                if (scherm_update == 1)
                {
                    oledClearScreen();
                    oledPrint(110, 0, actienummer, big);                                            //Actienummer rechtsboven weergeven
                    oledPrint(100, 3, "<--", big);
                    oledPrint(0, 0, "Rechts", small);
                    oledPrint(25, 3, "Vooruit", big);
                    oledPrint(0, 7, "Stop", small);
                    scherm_update = 0;
                }
                if ((ski == 1) && (scherm_update != 1))
                {
                    ski = 0;                                                                        //start knop is niet meer ingedrukt
                    scherm_update = 1;
                    Route[r] = 'D';                                                                 //Rechtdoor in array zetten op r
                    r++;
                }
            }

            if (pn == 11)
            {
                if (scherm_update == 1)
                {
                    oledClearScreen();
                    oledPrint(110, 0, actienummer, big);                                            //AActienummer rechtsboven weergeven
                    oledPrint(100, 3, "<--", big);
                    oledPrint(0, 0, "Vooruit", small);
                    oledPrint(25, 3, "Stop", big);
                    scherm_update = 0;
                }
                if ((ski == 1) && (scherm_update != 1))
                {
                    ski = 0;                                                                        //start knop is niet meer ingedrukt
                    Route[r] = 'S';                                                                 //stop in array zetten op r
                    UART_putc(SetDirections);                                                       //Boas, send the directions the robot has to follow
                    int i = 0;                                                                      //todo fix uart message directions
                    for(i=0;i <= r;i++){
                        UART_putc(Route[i]);
                    }

                    r = 42;                                                                         //klaar met invoermenu
                    scherm_update = 1;
                }
            }
        }
        startscherm = 1;
        invoerenroute = 0;                                                                          //uit de route selectie gaan
        wachten_op_start = 1;                                                                       //naar het volgende scherm
    }
    if (wachten_op_start == 1)
    {
        if ((ski == 1) && (scherm_update != 1))                                                     //Roy, Launch knop
        {
            ski = 0;                                                                                //start knop is niet meer ingedrukt
            motorsrunning = 1;                                                                      //Roy, motoren aan
            UART_putc(Start);                                                                       //Boas, Send a start message to the second msp
            UART_putc(Start);
            wachten_op_start = 0;
            Start_Timer(1);                                                                         //gerben, tijd bijhouden
            pn = 0;
            scherm_update = 1;

        }
        if ((scherm_update == 1) && (pn != 0))                                                      //Roy, scherm mag updaten
        {
            oledClearScreen();
            oledFillBox(0, 0, 127, 0, 0xFF);
            oledFillBox(107, 0, 127, 7, 0xFF);
            oledFillBox(0, 0, 20, 7, 0xFF);
            oledFillBox(0, 7, 127, 7, 0xFF);
            oledPrint(28, 1, "Ready to", big);                                                      //Roy, launch sherm
            oledPrint(36, 4, "Launch", big);
            scherm_update = 0;                                                                      //Roy, scherm mag niet updaten
        }
    }
}

void NFC_leds(unsigned char NFC_kleur)
{
    if (NFC_kleur == 'R')                                                                           //als de NFC tag rood leest
    {
        P1OUT |= (1 << 0);                                                                          //rood aan
        P2OUT &= ~(1 << 6 | 1 << 7);                                                                //groen en blauw uit
    }
    if (NFC_kleur == 'G')                                                                           //als de NFC tag groen leest
    {
        P2OUT |= (1 << 6);                                                                          //groen aan
        P1OUT &= ~(1 << 0);
        P2OUT &= ~(1 << 7);                                                                //rood en blauw uit
    }
    if (NFC_kleur == 'B')                                                                           //als de NFC tag blauw leest
    {
        P2OUT |= (1 << 7);                                                                          //blauw aan
        P1OUT &= ~(1 << 0);                                                                //rood en groen uit
        P2OUT &= ~(1<<6);
    }
}

void knipperlicht()
{
    if(direction == 'L')
    {
        knipper = 1;
    }
    if(direction == 'R')
    {
        knipper = 2;
    }
    if(direction == 'D')
    {
        knipper = 3;
    }
    if(((divideseconden%2) == 1) && ((seconden - starttellen) <= 3))                                                                           //als seconden oneven is
    {
        if(knipper == 1)                                                                            //als knipperlicht links aan moet
        {
            P1OUT |= (1<<4);
        }
        if(knipper == 2)                                                                            //als knipperlicht rechts aan moet
        {
            P1OUT |= (1<<3);
        }
        if(knipper == 3)
        {
            P1OUT |= (1<<5);
        }
        if((seconden - starttellen) >= 2)
        {
            starttellen = 0;
            knipper = 0;
            stoptellen = 0;
        }
    }
    else
    {
        P1OUT &= ~(1<<3);                                                                           //knipperlicht uit
        P1OUT &= ~(1<<4);                                                                           //knipperlicht uit
        P1OUT &= ~(1<<5);
    }
}
