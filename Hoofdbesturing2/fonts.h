/*
 * fonts.h
 *
 *  Created on: 7 mrt. 2018
 *      Author: VersD
 */

#ifndef FONTS_H_
#define FONTS_H_

/* itoa:  convert n to characters in s */
void itoa(int n, char s[]);

typedef enum {small,big} Font;

#endif /* FONTS_H_ */
