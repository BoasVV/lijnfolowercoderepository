#include <msp430.h> 
#include <stdint.h>
typedef enum
{
    Left, Right
} Side;
typedef enum
{
    Off, On
} Action;

void Motor(Side side, Action action, int pwm);

int PWMSpeed = 105;
int PWMlinks = 105;
int PWMrechts = 105;

char s = 0;

#pragma vector = PORT2_VECTOR
__interrupt void port2Interrupt(void)
{
    if (P2IFG & (1 << 0)) // linker sensor detecteert
    {
        Motor(Right,Off, PWMrechts);
        Motor(Left,On, PWMlinks);
        s = 1;
        P2IFG &= ~(1 << 0);
    }
    if (P2IFG & (1 << 3)) // rechter sensor detecteert
    {
        Motor(Right, On, PWMrechts);
        Motor(Left,Off, PWMlinks);
        s=2;
        P2IFG &= ~(1 << 3);
    }
    if (P2IFG & (1 << 1)) // midden sensor detecteert
    {
        if(s == 1){
            Motor(Right, Off, PWMSpeed);
            Motor(Left, On, PWMSpeed);
            s=2;
        }
        else if (s==2){
            Motor(Right, On, PWMSpeed);
            Motor(Left, Off, PWMSpeed);
            s=1;
        }
        P2IFG &= ~(1 << 1);
    }
}

void SetupRegisters(void) // sensoren
{
    // reset registers
    P2DIR &= ~(1 << 0); // Input gemaakt Midden sensor
    P2DIR &= ~(1 << 1); // Input gemaakt links en rechts sensoren
    P2DIR &= ~(1 << 3);

    P2IE |= (1 << 0);
    P2IE |= (1 << 1);
    P2IE |= (1 << 3);
    //P2IES |= ((1 << 0) | (1 << 3));
    P2IES &= ~(1 << 0);
    P2IES &= ~(1 << 1);
    P2IES &= ~(1 << 3);
    // end reset registers
}

void SetPWM(void)
{
    P2DIR |= (1 << 2) | (1 << 5); //PIN HOOG OUTPUT
    P2SEL |= (1 << 2) | (1 << 5); // OUTPUT CLOCK GAAT NAAR DE OUTPUT PIN

    if (CALBC1_1MHZ == 0xFF)
    {
        while (1)
            ; // Doe niets
    }

    /*
     * set clock frequentie on 1Mhz
     */
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    TA1CTL = TASSEL_2 | ID_3 | MC_1;

    TA1CCR0 = 300; // period time for pwm 1 and 2

    TA1CCTL1 = OUTMOD_7; // dutycycle pwm 1
    TA1CCR1 = 100;

    TA1CCTL2 = OUTMOD_7; // dutycycle pwm 2
    TA1CCR2 = 100;
}

/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	
	SetupRegisters();
	//SetPWM();
	P2DIR  |= (1<<2)|(1<<5);

	__enable_interrupt();
	int dir = 0;

	while(1){
	    /*if ((P2IN & (1 << 0))) // linker sensor detecteert
        {
            Motor(Left,Off, PWMlinks);
            Motor(Right,On, PWMrechts);
            dir = 1;
            //P2IFG &= ~(1 << 0);
        }
        if ((P2IN & (1 << 3))) // rechter sensor detecteert
        {
            Motor(Right, Off, PWMrechts);
            Motor(Left,On, PWMlinks);
            dir=2;
            //P2IFG &= ~(1 << 3);
        }
        if ((P2IN & (1 << 1))) // midden sensor detecteert
        {
            Motor(Right, On, PWMSpeed);
            Motor(Left, On, PWMSpeed);
            dir = 0;
            //P2IFG &= ~(1 << 1);
        }*/

        /*if((P2IN & ((1<<0)|(1<<3)|(1<<1))) == 0){ // alle sensoren 0
            Motor(Right, On, PWMSpeed);
            Motor(Left, On, PWMSpeed);
        }*/
	}

	return 0;
}

void Motor(Side side, Action action, int pwm)
{
    //if (pwm <= 300)
    //{
        if(side == Left){
            if(action == Off){
                P2OUT &= ~(1<<2);
            }
            else{
                P2OUT |= (1<<2);
            }
        }
        else if(side == Right){
            if(action == Off){
                P2OUT &= ~(1<<5);
            }
            else{
                P2OUT |= (1<<5);
            }
        }
        /*
        switch (side)
        {
        case Left:
            switch (action)
            {
            case Off:
                //TA1CCR1 = 0;
                P2OUT &= ~(1<<2);
                break;
            case On:
                P2OUT |= (1<<2);
                //TA1CCR1 = pwm;
                break;
            }
            break;
        case Right:
            switch (action)
            {
            case Off:
                P2OUT &= ~(1<<5);
                //TA1CCR2 = 0;
                break;
            case On:
                P2OUT |= (1<<5);
                //TA1CCR2 = pwm;
                break;
            }
            break;
        }*/
    //}
}
