/*
 * GPIO.h
 *
 *  Created on: 26 Mar 2019
 *      Author: BvV
 */

#ifndef INC_GPIO_H_
#define INC_GPIO_H_

#include <stdbool.h>
#include <stdint.h>

/*
 * Deze functie initialiseert de klok op 1, 8, 12 of 16MHz
 * Parameter:
 * - mhz: De in te stellen frequentie in MHz, kan 1, 8, 12 of 16 zijn.
 * Returnwaarde: (true of false) geeft aan of het gelukt is.
 */
bool zet_klok_op_MHz(uint8_t mhz);

/*
 * Deze functie initialiseert een GPIO pin als input of als output
 * Parameters:
 * - poort: het betreffende poortnummer (1 of 2)
 * - pin: het betreffende pinnummer (0 t/m 7 voor poort 1 of 0 t/m 5 voor poort 2)
 * - richting: de gewenste richting (input of output)
 */
typedef enum {input, output} Richting;
void zet_pin_richting(uint8_t poort, uint8_t pin, Richting richting);

/*
 * Deze functie maakt een GPIO output pin hoog of laag
 * Parameters:
 * - poort: het betreffende poortnummer (1 of 2)
 * - pin: het betreffende pinnummer (0 t/m 7 voor poort 1 of 0 t/m 5 voor poort 2)
 * - waarde: de waarde die naar de output pin wordt geschreven (laag of hoog)
 */
typedef enum {laag, hoog} Waarde;
void output_pin(uint8_t poort, uint8_t pin, Waarde waarde);

/*
 * Deze functie zet een interne weerstand bij een GPIO input pin aan of uit
 * Parameters:
 * - poort: het betreffende poortnummer (1 of 2)
 * - pin: het betreffende pinnummer (0 t/m 7 voor poort 1 of 0 t/m 5 voor poort 2)
 * - weerstand: het gewenste type (none, pull_up of pull_down)
 */
typedef enum {none, pull_down, pull_up} Weerstand;
void zet_interne_weerstand(uint8_t poort, uint8_t pin, Weerstand weerstand);

/*
 * Deze functie leest de waarde van een GPIO input pin
 * Parameters:
 * - poort: het betreffende poortnummer (1 of 2)
 * - pin: het betreffende pinnummer (0 t/m 7 voor poort 1 of 0 t/m 5 voor poort 2)
 * Returnwaarde: true als de input pin hoog is of false als de input pin laag is
 */
bool input_pin(uint8_t poort, uint8_t pin);

/*
 * Deze functie zet een adc waarde om in een spanning (in tienden)
 * Parameters:
 * - ADCValue: de waarde van de adc
 * - ref: de referentie spanning van de adc
 * - comma: de hoeveelheid getallen achter de comma default = 1000
 * Returnwaarde: de spanning in tienden Bijv. (201 voor 0.201 V )
 */
int conv(int ADCValue, float ref, int comma);

/*
 *
 */
typedef enum{rising,falling} EDGE;
void SetShiftRegisterOutput(int *LEPort, int LEPin, int *CLKPort, int CLKPin,
                            EDGE edge, int *DPort, int DPin, int Wvalue);


#endif /* INC_GPIO_H_ */
