/**
 *  @file i2c.h
 *  @brief Dit headerbestand bevat alle I2C prototypes
 *
 *  @author VersD
 *  @date   4 mei 2017
 */

#ifndef SRC_CUSTOMI2C_H_
#define SRC_CUSTOMI2C_H_

/**
 * \defgroup I2C
 * @brief I2C functionaliteiten
 *
 * De Universal Serial Communication Interface (USCI) module kan draaien in 3 modi:
 * I2C, SPI en UART. Met deze functies kun je de module in I2C modus gebruiken.
 *
 * De volgende stappen moeten worden ondernomen om I2C te kunnen gebruiken
 *
 * + Intialiseer de module @see i2cInitialize
 * + Stel de klokfrequentie in @see i2cSetClock
 * + Stel het I2C-adres in @see i2cSetAddress
 * + Stuur data op @see i2cSendBytes
 *
 * @{
 *
 */
#include "../../Lib/clocks.h"
#include "../../Lib/i2c.h"
#include <stdint.h>
#include <stdbool.h>
#define GetLength(x) (sizeof(x) / sizeof(x[0]))

//!De twee I2C modi
typedef enum {SINGLEMASTER=3,MULTIMASTER=4} Customi2cMode;
typedef enum {SHORT,LONG} i2cAddressLenght;

/*typedef enum {
    MASTER, SLAVE
} i2cmodes;*/
typedef enum
{
    i2c_IE1,
    i2c_IFG1,
    i2c_IE2,
    i2c_IFG2,
    i2c_ADC10CTL0,
    i2c_ADC10CTL1,
    i2c_ADC10DTC0,
    i2c_ADC10DTC1,
    i2c_ADC10AE0,
    i2c_ADC10MEM,
    i2c_ADC10SA,
    i2c_DCOCTL,
    i2c_BCSCTL1,
    i2c_BCSCTL2,
    i2c_BCSCTL3,
    i2c_CACTL1,
    i2c_CACTL2,
    i2c_CAPD,
    i2c_P1IN,
    i2c_P1OUT,
    i2c_P1DIR,
    i2c_P1IFG,
    i2c_P1IES,
    i2c_P1IE,
    i2c_P1SEL,
    i2c_P1SEL2,
    i2c_P1REN,
    i2c_P2IN,
    i2c_P2OUT,
    i2c_P2DIR,
    i2c_P2IFG,
    i2c_P2IES,
    i2c_P2IE,
    i2c_P2SEL,
    i2c_P2SEL2,
    i2c_P2REN

} registers;


void i2cInit(i2cMode mode, uint8_t OwnAddr);
void i2cEnableReceive();
void i2cEnableTransmit();
void i2cWriteRegister(uint8_t Register, uint8_t address, uint8_t value);
uint8_t i2cReadRegister(uint8_t Register, uint8_t address);

//bool ItemsInQueue();

//public functions
/*!
 * @brief Stel de USCI module in voor I2C gebruik (i.p.v. SPI)
 *
 * \param mode Stel de I2C-module in als SLAVE of MASTER. @see i2cSetMode()
 */
void Customi2cInitialize(Customi2cMode mode);

/*!
 * @brief Stel hiermee in of de module MASTER of SLAVE is op de I2C-bus.
 *
 * @code
 * i2cSetMode(MASTER)
 * @endcode
 *
 * \param mode Stel de I2C-module in als SLAVE of MASTER.
 *
 */
void Customi2cSetMode(Customi2cMode mode);

/*!
 * @brief Stel het adres in van deze I2C-module
 *
 * Elke I2C-module heeft een uniek adres op de bus.
 *
 * \param address Het 7 bits adres zonder read/write bit, volledig naar rechts geschoven. Dus bits 0-6 bevatten het adres.
 */
void Customi2cSetAddress(uint8_t address,i2cAddressLenght length);

void Customi2cSetOwnAddress(uint8_t address,i2cAddressLenght length);


//uint8_t * Deque();
//void Enque(uint8_t c,uint8_t address,i2cAddressLenght length);
//void Customi2cSend();
//void Customi2cSendByteQueue(uint8_t byte,uint8_t address,i2cAddressLenght length);
//void Customi2cSendBytesQueue(unsigned int number,const uint8_t bytes[],uint8_t address,i2cAddressLenght length);


/*!
 * @brief Zet een databyte in de transmit-buffer.
 *
 * Deze functie roept geen start of stop conditie aan. Het is dus te gebruiken wanneer
 * data verstuurd moet worden die run-time gegenereerd wordt. Zorg er dan wel voor dat
 * de communicatie wordt gestart en gestopt @see i2cStart @see i2cStop
 *
 * \param byte De 8 bits data om te laten versturen.
 */
void Customi2cSendByte(uint8_t byte);

/*!
 * @brief Start verbinding en stuur data
 *
 * Met deze functie kun je meerdere databytes versturen. De functie zet de verbinding
 * op door een startconditie te versturen, stuurt vervolgens alle meegegeven
 * data en eindigt met een stopconditie.
 *
 * @code
 * uint8_t data = {0x0A, 0xAC, 0xFF, 0x12};
 * i2cSendBytes(4, data);
 * @endcode
 *
 * \param number Het aantal bytes dat wordt verstuurd
 * \param bytes Een array met de te versturen bytes
 */
void Customi2cSendBytes(uint8_t number, const uint8_t bytes[]);

/*!
 * @brief Stel de I2C-klok in (wanneer in MASTER modus).
 *
 * In Master modus genereert de microcontroller de I2C-klok zelf.
 * Stel met deze functie in van welke klokbron de module moet draaien
 * en met welke waarde deze klok nog gedeeld moet worden. De uitkomst van
 * deze deling is de uiteindelijk gebruikte klokfrequentie.
 *
 * \param source De klokbron waarop de module zal draaien.
 * \param divider Deel de klokfrequentie door een 16 bits integer waarde.
 */
void Customi2cSetClock(clockSources source, uint16_t divider);

/*!
 * @brief Stuur een startconditie
 */
void Customi2cStart();

/*!
 * @brief Stuur een stopconditie
 */
void Customi2cStop();

/**
 * @}
 */

#endif /* SRC_CUSTOMI2C_H_ */
