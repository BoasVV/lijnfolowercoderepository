/*
 * i2c2.h
 *
 *  Created on: 17 May 2019
 *      Author: BvV
 */

#ifndef INC_I2C2_H_
#define INC_I2C2_H_

/**
 * \defgroup I2C
 * @brief I2C functionaliteiten
 *
 * De Universal Serial Communication Interface (USCI) module kan draaien in 3 modi:
 * I2C, SPI en UART. Met deze functies kun je de module in I2C modus gebruiken.
 *
 * De volgende stappen moeten worden ondernomen om I2C te kunnen gebruiken
 *
 * + Intialiseer de module @see i2cInitialize
 * + Stel de klokfrequentie in @see i2cSetClock
 * + Stel het I2C-adres in @see i2cSetAddress
 * + Stuur data op @see i2cSendBytes
 *
 * @{
 *
 */
#include "../../Lib/clocks.h"
#include <stdint.h>
/*
typedef enum {
    NONE,
    SetDirections,
    SetSpeed,
    Start,
    GetDistance,
    GetNextDirection,
    GetTwoComplements,
    GetColor,
    GetDone,
    Stop
} actions;*/

//!De twee I2C modi
typedef enum {MASTER,SLAVE} i2cMode;
typedef uint8_t* string;

//public functions
/*!
 * @brief Stel de USCI module in voor I2C gebruik (i.p.v. SPI)
 *
 * \param mode Stel de I2C-module in als SLAVE of MASTER. @see i2cSetMode()
 */
void i2cInitialize(i2cMode mode);

/*!
 * @brief Stel hiermee in of de module MASTER of SLAVE is op de I2C-bus.
 *
 * @code
 * i2cSetMode(MASTER)
 * @endcode
 *
 * \param mode Stel de I2C-module in als SLAVE of MASTER.
 *
 */
void i2cSetMode(i2cMode mode);

/*!
 * @brief Stel het adres in van deze I2C-module
 *
 * Elke I2C-module heeft een uniek adres op de bus.
 *
 * \param address Het 7 bits adres zonder read/write bit, volledig naar rechts geschoven. Dus bits 0-6 bevatten het adres.
 */
void i2cSetAddress(uint8_t address);

void i2cSetOwnAddress(uint8_t address);

/*!
 * @brief Zet een databyte in de transmit-buffer.
 *
 * Deze functie roept geen start of stop conditie aan. Het is dus te gebruiken wanneer
 * data verstuurd moet worden die run-time gegenereerd wordt. Zorg er dan wel voor dat
 * de communicatie wordt gestart en gestopt @see i2cStart @see i2cStop
 *
 * \param byte De 8 bits data om te laten versturen.
 */
void i2cSendByte(uint8_t byte);
uint8_t i2cReceive(void);
void i2cSetReceivedValue(uint8_t buff);

/*!
 * @brief Start verbinding en stuur data
 *
 * Met deze functie kun je meerdere databytes versturen. De functie zet de verbinding
 * op door een startconditie te versturen, stuurt vervolgens alle meegegeven
 * data en eindigt met een stopconditie.
 *
 * @code
 * uint8_t data = {0x0A, 0xAC, 0xFF, 0x12};
 * i2cSendBytes(4, data);
 * @endcode
 *
 * \param number Het aantal bytes dat wordt verstuurd
 * \param bytes Een array met de te versturen bytes
 */
void i2cSendBytes(uint8_t number, const uint8_t bytes[]);

/*!
 * @brief Stel de I2C-klok in (wanneer in MASTER modus).
 *
 * In Master modus genereert de microcontroller de I2C-klok zelf.
 * Stel met deze functie in van welke klokbron de module moet draaien
 * en met welke waarde deze klok nog gedeeld moet worden. De uitkomst van
 * deze deling is de uiteindelijk gebruikte klokfrequentie.
 *
 * \param source De klokbron waarop de module zal draaien.
 * \param divider Deel de klokfrequentie door een 16 bits integer waarde.
 */
void i2cSetClock(clockSources source, uint16_t divider);

/*!
 * @brief Stuur een startconditie
 */
void i2cStart();

/*!
 * @brief Stuur een stopconditie
 */
void i2cStop();

//uint8_t i2cSendMessage(actions action,uint8_t len, string message, uint8_t masteraddr);

/**
 * @}
 */

#endif /* INC_I2C2_H_ */
