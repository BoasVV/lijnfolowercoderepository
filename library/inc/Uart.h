/*
 * uart.h
 *
 *  Created on: 8 Jun 2019
 *      Author: model
 */

#ifndef INC_UART_H_
#define INC_UART_H_

#include <stdint.h>

typedef enum {
    NONE,
    SetDirections,
    SetSpeed,
    Start,
    GetDistance,
    GetNextDirection,
    GetTwoComplements,
    GetColor,
    GetDone,
    Stop
} actions;

static const unsigned long _dv[] = {
//  4294967296      // 32 bit unsigned max
   1000000000,     // +0
    100000000,     // +1
     10000000,     // +2
      1000000,     // +3
       100000,     // +4
//       65535      // 16 bit unsigned max
        10000,     // +5
         1000,     // +6
          100,     // +7
           10,     // +8
            1,     // +9
};

void UART_setup(void);

void UART_putc(unsigned char c);

void UART_puts(const char *str);

static void _xtoa(unsigned long x, const unsigned long *dp);

static void _puth(unsigned n);

void UART_printf(char *format, ...);
void UART_setReceive(uint8_t var);
uint8_t UART_receive();

uint8_t* UARTSendMessage(actions action, uint8_t* message);

#endif /* INC_UART_H_ */
