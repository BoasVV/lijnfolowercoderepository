/*
 * GPIO.c
 *
 *  Created on: 26 Mar 2019
 *      Author: BvV
 */

#include <msp430.h>
#include "GPIO.h"


bool zet_klok_op_MHz(uint8_t mhz)
{
    DCOCTL = 0;

    if (CALBC1_1MHZ == 0xFF || CALBC1_8MHZ == 0xFF || CALBC1_12MHZ == 0xFF
            || CALBC1_16MHZ == 0xFF)
    {
        return false;
    }

    switch (mhz)
    {
    case 1:
        BCSCTL1 = CALBC1_1MHZ; // Set range
        DCOCTL = CALDCO_1MHZ;  // Set DCO step + modulation */
        break;
    case 8:
        BCSCTL1 = CALBC1_8MHZ; // Set range
        DCOCTL = CALDCO_8MHZ;  // Set DCO step + modulation */
        break;
    case 12:
        BCSCTL1 = CALBC1_12MHZ; // Set range
        DCOCTL = CALDCO_12MHZ;  // Set DCO step + modulation */
        break;
    case 16:
        BCSCTL1 = CALBC1_16MHZ; // Set range
        DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */
        break;
    default:
        return false;
    }

    return true;
}

void zet_pin_richting(uint8_t poort, uint8_t pin, Richting richting)
{
    if (poort == 1)
    {
        if (pin <= 7)
        {
            if (richting == input)
            {
                P1DIR &= ~(1 << pin);
            }
            else if (richting == output)
            {
                P1DIR |= 1 << pin;
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            if (richting == input)
            {
                P2DIR &= ~(1 << pin);
            }
            else if (richting == output)
            {
                P2DIR |= 1 << pin;
            }
        }
    }
}

void output_pin(uint8_t poort, uint8_t pin, Waarde waarde)
{
    if (poort == 1)
    { // check wich port is selected
        if (pin <= 7)
        { // check if the pin is inside the range of the port
            if ((P1DIR & (1 << pin)) == (1 << pin))
            { // if the pin is configured as output than this function is able to set the output
                if (waarde == hoog)
                { // check if the pin must be set high
                    P1OUT |= 1 << pin; // set the pin high
                }
                else if (waarde == laag)
                { // check if the pin must be set low
                    P1OUT &= ~(1 << pin); // set the pin low
                }
            }
        }
    }
    else if (poort == 2)
    { // check wich port is selected
        if (pin <= 5)
        { // check if the pin is inside the range of the port
            if ((P2DIR & (1 << pin)) == (1 << pin))
            { // if the pin is configured as output than this function is able to set the output
                if (waarde == hoog)
                { // check if the pin must be set high
                    P2OUT |= 1 << pin; // set the pin high
                }
                else if (waarde == laag)
                { // check if the pin must be set low
                    P2OUT &= ~(1 << pin); // set the pin low
                }
            }
        }
    }
}

void zet_interne_weerstand(uint8_t poort, uint8_t pin, Weerstand weerstand)
{
    if (poort == 1)
    {
        if (pin <= 7)
        {
            if ((P1DIR & (1 << pin)) == 0)
            {
                if (weerstand == none)
                {
                    P1REN &= ~(1 << pin);
                }
                else if (weerstand == pull_down)
                {
                    P1REN |= 1 << pin;
                    P1OUT &= ~(1 << pin);
                }
                else if (weerstand == pull_up)
                {
                    P1REN |= 1 << pin;
                    P1OUT |= 1 << pin;
                }
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            if ((P1DIR & (1 << pin)) == 0)
            {
                if (weerstand == none)
                {
                    P2REN &= ~(1 << pin);
                }
                else if (weerstand == pull_down)
                {
                    P2REN |= 1 << pin;
                    P2OUT &= ~(1 << pin);
                }
                else if (weerstand == pull_up)
                {
                    P2REN |= 1 << pin;
                    P2OUT |= 1 << pin;
                }
            }
        }
    }
}

bool input_pin(uint8_t poort, uint8_t pin)
{
    if (poort == 1)
    {
        if (pin <= 7)
        {
            if (P1DIR & (1 << pin) == 0)
            {
                if (P2OUT & (1 << pin) == 0)
                {
                    if (((P1IN & (1 << pin)) >> pin) == hoog)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (((P1IN & (1 << pin)) >> pin) == hoog)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            if (P2DIR & (1 << pin) == 0)
            {
                if (P2OUT & (1 << pin) == 0)
                {
                    if (((P2IN & (1 << pin)) >> pin) == hoog)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (((P2IN & (1 << pin)) >> pin) == hoog)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}


void SetShiftRegisterOutput(int *LEPort, int LEPin, int *CLKPort, int CLKPin, EDGE edge,
                            int *DPort, int DPin, int Wvalue){
    *LEPort &= ~(1<<LEPin);
    int i;
    for(i = 14; i > 0;i--){
        if(edge == falling) *CLKPort |= (1<<CLKPin);
        else *CLKPort &= ~(1<<CLKPin);

        if((Wvalue & 1<<i)){
            *DPort |= (1 << DPin);
        }
        else{
            *DPort &= ~(1<<DPin);
        }

        if(edge == rising) *CLKPort |= (1<<CLKPin);
        else *CLKPort &= ~(1<<CLKPin);
    }

    *LEPort |= (1<<LEPin);
}

int conv(int ADCValue, float ref, int comma)
{
    float value = (ADCValue / (float) 1023) * (ref * comma);
    if (value - (int) value > 0.5)
    {
        return (int) value + 1;
    }
    return (int) (value);
}

