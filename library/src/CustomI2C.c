///*
// * CustomI2C.c
// *
// *  Created on: 8 May 2019
// *      Author: BvV
// */
//
//#include "CustomI2C.h"
//#include "../../Lib/i2c.h"
//#include <msp430.h>
//#include <stdbool.h>
//
//static uint8_t _i2c_receiveRegister = 0;
//
//#pragma vector = USCIAB0TX_VECTOR
//__interrupt void ISR_I2C_TX(void)
//{
//    if(IFG2 & UCB0TXIFG){
//        switch (_i2c_receiveRegister)
//        {
//        case 1:
//            UCB0TXBUF = 0;
//            //P1OUT = 0;
//            break;
//        default:
//            UCB0TXBUF = 3;
//            //UCB0CTL1 |= UCTXNACK;
//        }
//        _i2c_receiveRegister = 0;
//        //IFG2 &= ~UCB0TXIFG;
//
//    }
//    //if(IFG2 & UCB0RXIFG){
//        //_i2c_receiveRegister = UCB0RXBUF;
//        //P1OUT = _i2c_receiveRegister;
//        //IFG2 &= ~UCB0RXIFG;
//    //}
//}
//
//#pragma vector = USCIAB0RX_VECTOR
//__interrupt void ISR_I2C_RX(void)
//{
//    if (_i2c_receiveRegister == 0)
//    {
//        _i2c_receiveRegister = UCB0RXBUF;
//    }
//    else
//    {
//        uint8_t newValue = UCB0RXBUF;
//        switch(_i2c_receiveRegister){
//        default:
//            //UCB0CTL1 |= UCTXNACK;
//        }
//        _i2c_receiveRegister = 0;
//    }
//}
//
//void i2cInit(i2cMode mode, uint8_t OwnAddr)
//{
//    UCB0CTL1 |= UCSWRST;
//
//    switch (mode)
//    {
//    case MASTER:
//
//        break;
//    case SLAVE:
//        UCB0CTL0 |= UCMODE_3; // i2c mode
//        UCB0CTL0 |= UCSYNC; // sync mode
//        UCB0CTL0 &= ~UCMST; // slave
//        UCB0CTL1 &= ~UCTR; // receive mode
//        UCB0I2COA = OwnAddr; // set address
//        UCB0CTL0 &= ~UCA10; // 7 bit address;
//        break;
//    }
//    P1SEL |= (3 << 5);
//    P1SEL2 |= (3 << 5);
//    UCB0CTL1 &= ~UCSWRST;
//}
//
//void i2cEnableReceive()
//{
//    __disable_interrupt();
//    IE2 |= UCB0RXIE;
//    IFG2 |= UCB0RXIE;
//    __enable_interrupt();
//}
//
//void i2cEnableTransmit()
//{
//    //__disable_interrupt();
//    //IE2 |= UCB0TXIE;
//    //IFG2 |= UCB0TXIE;
//    //__enable_interrupt();
//}
//
//void i2cWriteRegister(uint8_t Register, uint8_t address, uint8_t value)
//{
//    //versturen, dus write bit moet aan
//        UCB0CTL1 |= UCTR;
//    //UCB0CTL1 |= UCSWRST;
//    //UCB0CTL1 &= ~UCTR;
//    //UCB0CTL1 &= ~UCSWRST;
//    i2cSetAddress(address);
//    i2cStart();
//    while(!(IFG2 & UCB0TXIFG));
//    UCB0TXBUF = Register;
//    while(!(UCB0CTL1 & UCTXSTT));
//    int whilestatus = (IFG2 & UCB0TXIFG);
//    while(!whilestatus)
//        whilestatus = (IFG2 & UCB0TXIFG); //wachten op ifg
//    i2cStop();
//    //i2cSendBytes(1,Register);
//    //UCB0CTL1 |= UCSWRST;
//    //UCB0CTL1 |= UCTR;
//    //UCB0CTL1 &= ~UCSWRST;
//    //i2cSendBytes(1,value);
//    i2cStart();
//    UCB0TXBUF = value;
//    while(!(IFG2 & UCB0TXIFG)); //wachten op ifg
//    i2cStop();
//
//}
//
//uint8_t i2cReadRegister(uint8_t Register, uint8_t address)
//{
//    UCB0CTL1 |= UCSWRST;
//    UCB0CTL1 &= ~UCTR;
//    UCB0CTL1 &= ~UCSWRST;
//    i2cSetAddress(address);
//    i2cStart();
//    i2cSendByte(Register);
//    i2cStop();
//    return 0;
//}
///*
//#include <msp430.h>
//#include <stdint.h>
//
//#include "i2c.h"
//#include "gpio.h"
//
//i2cMode i2cStatus = MASTER;
//
//void i2cStart()
//{
//    //start
//    UCB0CTL1 |= UCTXSTT;
//}
//
//void i2cStop()
//{
//    //stop
//    UCB0CTL1 |= UCTXSTP;
//    while(UCB0CTL1 & UCTXSTP); //wachten op een acknowledge
//}
//
//void i2cInitialize(i2cMode mode)
//{
//    //zet de i2c module in reset modus
//    setBits(UCB0CTL1, UCSWRST);
//
//    //set to synchronous I2C mode
//    setBits(UCB0CTL0,  UCMODE_3 | UCSYNC);
//
//    //i2c functie selecteren pinnetjes
//    gpioSetFunction(1, 6, SECONDARY);
//    gpioSetFunction(1, 7, SECONDARY);
//
//    //Bij 16mhz levert dit 800khz.
//    //Sneller dan gespecificeerd maar
//    //lijkt zonder problemen te werken
//    i2cSetClock(SMCLK, 20);
//
//    i2cSetMode(mode);
//
//    //haal uit reset modus
//    clearBits(UCB0CTL1, UCSWRST);
//
//
//}
//
//void i2cSetClock(clockSources source, uint16_t divider)
//{
//    //klok selecteren
//    clearBits(UCB0CTL1, (0x3<<6));
//    setBits(UCB0CTL1, (source<<6));
//
//    //klokdeler selecteren voor frequentie
//    UCB0BR0 = (divider & 0x00FF);
//    UCB0BR1 = (divider>>8);
//}
//
//void i2cSetMode(i2cMode mode)
//{
//    i2cStatus = mode;
//
//    switch(mode)
//    {
//        case MASTER:
//            setBits(UCB0CTL0,   UCMST);
//            break;
//
//        case SLAVE:
//        default:
//            clearBits(UCB0CTL0,   UCMST);
//    }
//}
//
//void i2cSetAddress(uint8_t address)
//{
//    UCB0I2CSA = address;
//}
//
//void i2cSendBytes(uint8_t number, const uint8_t bytes[])
//{
//    //versturen, dus write bit moet aan
//    UCB0CTL1 |= UCTR;
//
//    //start conditie op de bus zetten
//    i2cStart();
//
//    uint8_t byteCounter;
//    for(byteCounter=0; byteCounter<number; byteCounter++)
//    {
//        UCB0TXBUF = bytes[byteCounter];
//        while(!(IFG2 & UCB0TXIFG)); //wachten op ifg
//    }
//
//    i2cStop();
//}
//
//void i2cSendByte(uint8_t data)
//{
//    UCB0TXBUF = data;
//    while(!(IFG2 & UCB0TXIFG)); //wachten op ifg
//}*/
//
///*
// #define QueueLength 100
//
// uint8_t queue[QueueLength][2];
// unsigned int queueStart = 0;
// unsigned int queueEnd = 0;
//
// #pragma vector = USCIAB0TX_VECTOR
// __interrupt void i2cSend(){
// if(IFG2 & UCA0TXIFG){
// IFG2 &= ~UCA0TXIFG;
// }
// _NOP();
// }
//
// bool ItemsInQueue(){
// if(queueStart == queueEnd){
// return false;
// }
// else return true;
// }
//
// void Customi2cInitialize(Customi2cMode mode){
//
// P1SEL |= (1<<6) | (1<<7);
// P2SEL |= (1<<6) | (1<<7);
//
// UCB0CTL1 |= UCSWRST;
// UCB0CTL0 = UCMODE_3;
// IE2 |= UCB0TXIE;
// Customi2cSetMode(mode);
// Customi2cSetClock(0,4);
// UCB0CTL1 &= ~UCSWRST;
// }
//
// void Customi2cSetMode(Customi2cMode mode){
// UCB0CTL1 |= UCSWRST;
// switch(mode){
// case SINGLEMASTER:
// UCB0CTL1 |= UCSYNC; //sync mode
// UCB0CTL1 |= UCMST; // Master mode
// UCB0CTL1 &= ~UCMM; // disable multimaster
// break;
// case MULTIMASTER:
// UCB0CTL1 |= UCSYNC; //sync mode
// UCB0CTL1 |= UCMST; // Master mode
// UCB0CTL1 |= UCMM; // enable multimaster
// break;
// case SLAVE:
// UCB0CTL1 |= UCSYNC; // sync mode
// UCB0CTL1 &= ~UCMST; // slave mode
// UCB0CTL1 &= ~UCMM; // single master environment
//
// UCB0CTL1 &= ~UCTR; // for a slave device always starting in receiving mode
// break;
// }
// UCB0CTL1 &= ~UCSWRST;
// }
//
// void Customi2cSetAddress(uint8_t address,i2cAddressLenght length){
// UCB0CTL1 |= UCSWRST;
// if(length == SHORT){ // 7 bits right justified address
// UCB0CTL0 &= ~UCSLA10; // set address to 7 bits
// UCB0I2CSA = address & ~(0xff80); // convert the address to 7 bits
// }
// else if(length == LONG){ // 10 bits right justified address
// UCB0CTL0 |= UCSLA10; // set address to 10 bits
// UCB0I2CSA = address & ~(0xfc00); // convert the address to 10 bits
// }
// UCB0CTL1 &= ~UCSWRST;
// }
//
// void Customi2cSetOwnAddress(uint8_t address,i2cAddressLenght length){
// UCB0CTL1 |= UCSWRST;
// if(length == SHORT){ // 7 bits right justified address
// UCB0CTL0 &= ~UCA10; // set address to 7 bits
// UCB0I2COA = address & ~(UCGCEN | 0x7f80); // convert the address to 7 bits
// }
// else if(length == LONG){ // 10 bits right justified address
// UCB0CTL0 |= UCA10; // set address to 10 bits
// UCB0I2COA = address & ~(UCGCEN | 0x7c00); // convert the address to 10 bits
// }
// UCB0CTL1 &= ~UCSWRST;
//
// }
//
// uint8_t * Deque(){
// if(ItemsInQueue()){
// static uint8_t data[2];
// data[0] = queue[queueStart][0]; // byte
// data[1] = queue[queueStart][1]; // address
// queueStart++;
//
// if(queueStart > QueueLength){
// queueStart = 0;
// }
// return data;
// }
// else return 0;
// }
//
// void Enque(uint8_t c,uint8_t address,i2cAddressLenght length){
// queueEnd++;
//
// //overwrite char
// if(queueEnd == queueStart){
// queueStart++;
// }
//
// // circulair buffer
// if(queueStart > QueueLength){
// queueStart = 0;
// }
// if(queueEnd > QueueLength){
// queueEnd = 0;
// }
//
// // write char
// queue[queueEnd][0] = c;
// queue[queueEnd][1] = address;
// }
//
//
//
// void Customi2cSend(){
// if(ItemsInQueue()){
// uint8_t * c = Deque();
// i2cSetAddress(c[1]);
// //Customi2cSetAddress(c[1],SHORT);
// i2cStart();
// Customi2cSendByte(c[0]);
// i2cStop();
// }
// }
//
// void Customi2cSendByteQueue(uint8_t byte,uint8_t address,i2cAddressLenght length){
// Enque(byte,address,length);
// }
//
// void Customi2cSendBytesQueue(unsigned int number,const uint8_t bytes[],uint8_t address,i2cAddressLenght length){
// unsigned int i = number; // set the size for the loop
// for(; i > 0; i--){
// Enque(bytes[i],address,length); // enque the data
// }
// }
//
// void Customi2cSendByte(uint8_t byte){
// i2cSendByte(byte);
// }
//
// void Customi2cSendBytes(uint8_t number, const uint8_t bytes[]){
// i2cStart();
// i2cSendBytes(number,bytes);
// i2cStop();
// }
//
// void Customi2cSetClock(clockSources source, uint16_t divider){
// i2cSetClock(source,divider);
// }
// */
