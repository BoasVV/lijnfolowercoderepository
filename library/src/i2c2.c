/*
 * i2c2.c
 *
 *  Created on: 17 May 2019
 *      Author: BvV
 */


#include <msp430.h>
#include <stdint.h>

#include "../inc/i2c2.h"
#include "../../Lib/gpio.h"

i2cMode i2cStatus = MASTER;
static const int timeout = 1000;
int trys = 0;
void i2cStart()
{
    //start
    UCB0CTL1 |= UCTXSTT;
}

void i2cStop()
{
    //stop
    if(!(UCB0CTL1 & UCTR)){
        //UCB0CTL1 |= UCTXNACK;
    }
    UCB0CTL1 |= UCTXSTP;
    while((UCB0CTL1 & UCTXSTP)&& (trys!=timeout))
        trys++; //wachten op een acknowledge
    trys=0;
}

void i2cInitialize(i2cMode mode)
{
    //zet de i2c module in reset modus
    setBits(UCB0CTL1, UCSWRST);

    //set to synchronous I2C mode
    setBits(UCB0CTL0,  UCMODE_3 | UCSYNC);

    //i2c functie selecteren pinnetjes
    gpioSetFunction(1, 6, SECONDARY);
    gpioSetFunction(1, 7, SECONDARY);

    //Bij 16mhz levert dit 800khz.
    //Sneller dan gespecificeerd maar
    //lijkt zonder problemen te werken
    i2cSetClock(SMCLK, 20);

    i2cSetMode(mode);

    //haal uit reset modus
    clearBits(UCB0CTL1, UCSWRST);
}

void i2cSetClock(clockSources source, uint16_t divider)
{
    //klok selecteren
    clearBits(UCB0CTL1, (0x3<<6));
    setBits(UCB0CTL1, (source<<6));

    //klokdeler selecteren voor frequentie
    UCB0BR0 = (divider & 0x00FF);
    UCB0BR1 = (divider>>8);
}

void i2cSetMode(i2cMode mode)
{
    i2cStatus = mode;

    switch(mode)
    {
        case MASTER:
            setBits(UCB0CTL0,   UCMST);
            break;

        case SLAVE:
            clearBits(UCB0CTL0,   UCMST);
            clearBits(UCB0CTL1,   UCTR);
            break;
        default:
            clearBits(UCB0CTL0,   UCMST);
    }
}

void i2cSetAddress(uint8_t address)
{
    UCB0I2CSA = address;
}

void i2cSetOwnAddress(uint8_t address)
{
    UCB0I2COA = address;
}

void i2cSendBytes(uint8_t number, const uint8_t bytes[])
{
    //versturen, dus write bit moet aan
    UCB0CTL1 |= UCTR;

    //start conditie op de bus zetten
    i2cStart();

    uint8_t byteCounter;
    for(byteCounter=0; byteCounter<number; byteCounter++)
    {
        UCB0TXBUF = bytes[byteCounter];
        while((!(IFG2 & UCB0TXIFG))&&trys != timeout)
            trys++; //check tx buff is empty
        trys=0;
    }

    i2cStop();
}

void i2cSendByte(uint8_t data)
{
    UCB0TXBUF = data;
    while((!(IFG2 & UCB0TXIFG))&&trys != timeout)
        trys++; //Check if tx buff is empty
    trys=0;
}

int i2c_buff = -1;

void i2cSetReceivedValue(uint8_t buff){
    i2c_buff = buff;
}

uint8_t i2cReceive(void){
    int ret = -1;
    do{
        ret = i2c_buff;
        trys++;
    }
    while(ret == -1 && trys!= timeout);
    i2c_buff = -1;
    trys=0;

    return ret;
}
/*
uint8_t recvData[50];
uint8_t i2cSendMessage(actions action, uint8_t len, string message,uint8_t masteraddr){
    string ret = "";

    uint8_t byteCounter;
    switch(action){
    case SetDirections:
    case SetSpeed:
    case Start:
    case Stop:
        UCB0CTL1 |= UCTR;
        i2cStart();
        i2cSendByte(action);
        i2cSendByte(len);

        for(byteCounter=0; byteCounter<len; byteCounter++)
        {
            UCB0TXBUF = message[byteCounter];
            while((!(IFG2 & UCB0TXIFG))&&trys != timeout)
                trys++; //check tx buff is empty
            trys=0;
        }

        break;
    case GetDistance:
    case GetNextDirection:
    case GetTwoComplements:
    case GetColor:
    case GetDone:
    {
        UCB0CTL1 |= UCTR;
        i2cStart();
        i2cSendByte(action);
        i2cStop();
        UCB0CTL1 &= ~UCTR;
        i2cStart();
        //i2cReceive();
        //for(byteCounter = 0; byteCounter < 1; byteCounter++){
            //recvData[byteCounter] = i2cReceive();
        //}
        recvData[0] = i2cReceive();
        ret = recvData;

        break;
    }
    default:
        break;
    }

    i2cStop();
    UCB0CTL1 |= UCTR;

    return ret[0];
}*/
